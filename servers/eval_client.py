#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" Client Code for testing evaluation module.

    HISTORY
    Ver 0.1

"""
import sys
import argparse
import socket
import configparser
from lib.posco import recv_all
from lib.posco import get_datetime
from sys_component.streams.rh_macro_test_stream_minds_190514 import stream as rh_stream

RECV_BUFF_SIZE = 1024

RH_TEST = False
CVT_TEST = False
RH_PLUS_TEST = False

RH_M5_TEST = False
RH_M6_TEST = True


CMD_CHECK = False
CMD_STOP = False

# if RH_TEST:
#     stream = rh_stream
# elif CVT_TEST:
#     stream = cvt_stream
# elif RH_PLUS_TEST:
#     stream = rh_plus_stream
# elif RH_M6_TEST:
#

stream = rh_stream

if CMD_CHECK:
    stream = '{"cmd": "Check"}'
if CMD_STOP:
    stream = '{"cmd": "Stop"}'


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--cat", required=True, help="Category, rh, cvt, or rh+")
    parser.add_argument("--cfg_ini_file", required=True, help="configuration ini file")

    args = parser.parse_args()
    cfg = configparser.ConfigParser()
    cfg.read(args.cfg_ini_file)

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (cfg['EVAL_SERVER']['ip'], int(cfg['EVAL_SERVER']['port']))
    print(" EvalClient> connecting to %s port %s..." % server_address)
    try:
        sock.connect(server_address)
    except ConnectionRefusedError:
        print(" @ ConnectionRefusedError")
        sys.exit()

    # if len(stream) > 100:
    #     stream1 = "{}{}".format(cfg['EVAL_CLIENT']['name'], stream[8:])
    # else:
    stream1 = stream

    try:
        print(" EvalClient> {} : sending \"{}\"".format(get_datetime(), stream1))
        sock.sendall(stream1.encode('utf-8'))
    finally:
        print(" EvalClient> {} : closing socket...".format(get_datetime()))
        sock.close()

    if len(stream) < 100:
        sock_rep = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (cfg['CHECK_SERVER']['ip'], int(cfg['CHECK_SERVER']['port']))
        print("Starting up on %s port %s..." % server_address)
        sock_rep.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock_rep.bind(server_address)
        sock_rep.listen(1)

        print("Waiting for a connection...")
        connection, client_address = sock_rep.accept()
        print("Connection for {}...".format(client_address))

        try:
            str_dat = recv_all(connection).decode('utf-8')
            print("SOCK_REQ: {}".format(str_dat))
        finally:
            connection.close()

    return


if __name__ == "__main__":
    if len(sys.argv) == 1:
        if RH_TEST:
            sys.argv.extend(["--cat", "rh",  "--cfg_ini_file",  "../sys_temp/rh_cfg_sa_posco.ini"])
        elif CVT_TEST:
            sys.argv.extend(["--cat", "cvt", "--cfg_ini_file", "../sys_temp/cvt_cfg_sa_minds.ini"])
        elif RH_PLUS_TEST:
            sys.argv.extend(["--cat", "rh_plus", "--cfg_ini_file", "../sys_temp/rh_plus_cfg_sa_posco.ini"])
        elif RH_M5_TEST:
            sys.argv.extend(["--cat", "RH_M5", "--cfg_ini_file", "../sys_component/config_networking/rh_m5_cfg_sa_minds.ini"])
        elif RH_M6_TEST:
            sys.argv.extend(["--cat", "RH_M6", "--cfg_ini_file", "../sys_component/config_networking/rh_m6_cfg_sa_minds.ini"])
    main()