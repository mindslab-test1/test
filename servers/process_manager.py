#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
from __future__ import division
import os
import argparse
import configparser
import platform
import psutil
from signal import SIGTERM  # or SIGKILL
import socket
import errno
import sys
import time
from lib import HoonUtils as utils


CMD_STT = ""
CMD_END = ""
os_type = platform.system()
if os_type == "Darwin":
    CMD_STT += "nohup "
    CMD_END += " > /dev/null 2>&1 &"
elif os_type == "Linux":
    CMD_STT += "nohup "
    CMD_END += " > /dev/null 2>&1 &"
elif os_type == "Windows":
    CMD_STT += "start /min "
else:
    CMD_STT += "nohup "
    CMD_END += " > /dev/null 2>&1 &"


class Processes:

    def __init__(self, ini=None):
        self.process_number = 0
        self.processes = []
        if ini:
            self.initialize(ini)

    def initialize(self, ini):
        self.process_number = int(ini['GENERAL']['process_number'])
        for i in range(self.process_number):
            self.processes.append(self.Process(sect=ini['PROCESS_{:d}'.format(i+1)]))

    class Process:
        def __init__(self, sect=None):
            self.desc = ""
            self.port = -1
            self.start_script = ""
            if sect:
                self.initialize(sect)

        def initialize(self, sect):
            self.desc = sect["description"].strip()
            self.port = int(sect["port"])
            self.start_script = sect["start_script"].strip() + ' --port ' + sect["port"]

        def is_port_in_use(self):
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                # return s.connect_ex(('localhost', self.port)) == 0
                try:
                    s.bind(('localhost', self.port))
                except socket.error as e:
                    if e.errno == errno.EADDRINUSE:
                        return True
                    else:
                        print(e)
                        return True
                return False

        def run(self):
            if self.is_port_in_use():
                print(" - Already running and kill it")
                self.kill()
            os.system(CMD_STT + self.start_script + CMD_END)
            time.sleep(1)
            print(" > Running {} process with {:d} port\n".format(self.desc, self.port))

        def check(self):
            if self.is_port_in_use():
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                server_address = ('localhost', self.port)
                print('\n > Connecting to %s port %s' % server_address)
                sock.connect(server_address)
                try:
                    message = '{"cmd" : "check"}'
                    sock.sendall(message.encode('utf-8'))
                    data = sock.recv(256)
                    print(" > {}".format(data.decode('utf-8')))
                    print(" @ Process {} is active.\n".format(self.desc))
                finally:
                    sock.close()
            else:
                print(" @ Process {} is not active.".format(self.desc))

        def kill(self):
            for proc in psutil.process_iter():
                try:
                    for conns in proc.connections(kind='inet'):
                        if conns.laddr[1] == self.port:
                            proc.send_signal(SIGTERM)  # or SIGKILL
                            print(" > Killed the process {} using {:d} port\n".format(self.desc, self.port))
                            time.sleep(1)
                except psutil.AccessDenied:
                    pass
                except psutil.ZombieProcess:
                    pass
                except Exception as e:
                    print(e)

    def get_list(self):
        print("\n # Process List")
        for idx, process in enumerate(self.processes):
            use_ = self.processes[idx].is_port_in_use()
            print(" {:2d}. {} : {:d} - {}".format(idx+1, process.desc, process.port, use_))


def print_main_menu(processes):
    processes.get_list()
    print("\n # Process Manager Main Menu")
    print(" 1. Start")
    print(" 2. Check")
    print(" 3. Stop")
    print(" 0. Exit")


def run_start(processes):
    while True:
        ans = input(" Enter process index, 0 for all, or -1 to exit : ")
        ans = int(ans)
        if ans == -1:
            return True
        if ans == 0:
            for i in range(processes.process_number):
                print(" = Start to run {}...".format(processes.processes[i].desc))
                processes.processes[i].run()
        elif 0 < ans <= len(processes.processes):
            processes.processes[ans - 1].run()
        else:
            print(" @ Invalid process number, {:d}".format(ans))


def run_check(processes):
    while True:
        ans = input(" Enter process index to check or -1 to exit : ")
        try:
            ans = int(ans)
        except ValueError:
            print(" @ Error, invalid input number, {}".format(ans))
            continue
        if ans == -1:
            return True
        if ans == 0:
            for i in range(processes.process_number):
                print(" > Start to check {}...".format(processes.processes[i].desc))
                processes.processes[i].check()
        elif 0 < ans <= len(processes.processes):
            processes.processes[ans - 1].check()
        else:
            print(" @ Invalid process number, {:d}".format(ans))


def run_stop(processes):
    while True:
        ans = input(" Enter process index, 0 for all, or -1 to exit : ")
        ans = int(ans)
        if ans == -1:
            return True
        if ans == 0:
            for i in range(processes.process_number):
                print(" = Start to stop {}...".format(processes.processes[i].desc))
                processes.processes[i].kill()
        elif 0 < ans <= len(processes.processes):
            processes.processes[ans - 1].kill()
        else:
            print(" @ Invalid process number, {:d}".format(ans))


def main(args):
    """
    Main of Process Manager.
    :params args:
    :return:
    """
    this_folder = os.path.dirname(os.path.abspath(__file__))
    ini = configparser.ConfigParser()
    ini.read(os.path.join(this_folder, args.ini))
    ini = utils.remove_comments_in_ini(ini, delimiter="###")
    processes = Processes(ini=ini)

    while True:
        print_main_menu(processes)
        ans = input(" \n Enter command, start(1), check(2), stop(3), or exit(0) ? ")
        try:
            ans = int(ans[0])
        except ValueError:
            print("\n You entered {}. Please enter 0, 1, 2, or 3. Thanks".format(ans))
            continue

        if ans == 0:
            print("\n # Bye")
            break
        elif ans == 1:
            run_start(processes)
        elif ans == 2:
            run_check(processes)
        elif ans == 3:
            run_stop(processes)
        else:
            print("\n You entered {:d}. Please enter 0, 1, 2, or 3. Thanks".format(ans))


def parse_arguments(argv):
    """
    Parse arguments.

    :param argv:
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--ini", required=True, help="ini filename")
    return parser.parse_args(argv)


SELF_TEST_ = True
INI_FNAME = "../servers/process_manager.ini"


if __name__ == "__main__":

    if len(sys.argv) == 1:
        if SELF_TEST_:
            sys.argv.extend(["--ini", INI_FNAME])
        else:
            sys.argv.extend(["--help"])

    main(parse_arguments(sys.argv[1:]))
