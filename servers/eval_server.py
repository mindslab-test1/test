import argparse
import configparser
import socket
import sys
import threading

from lib import sys_lib as system_lib
from lib import networking
from models import est_eval


DB_OP_ = True


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--cat", required=True, help="Category, rh, cvt or rh+")
    parser.add_argument("--cfg_ini_file", required=True, help="configuration ini file")
    parser.add_argument("--log_no_console_", default=False, action='store_true', help="no console flag")
    parser.add_argument("--ml_ini_file", required=True, help="machine learning ini file")

    return parser


def main():
    parser = create_parser()
    args = parser.parse_args()

    ini = configparser.ConfigParser()
    ini.read(args.cfg_ini_file)

    logger = system_lib.setup_logger(args.cat + '_EVAL',
                                     ini['LOG']['eval_prefix'],
                                     folder=ini['LOG']['folder'],
                                     console_=(not args.log_no_console_))

    logger.info("START " + args.cat + "_EVAL server")

    sock_req = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (ini['EVAL_SERVER']['ip'], int(ini['EVAL_SERVER']['port']))
    logger.info("Starting up on %s port %s..." % server_address)
    sock_req.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock_req.bind(server_address)
    sock_req.listen(5)
    while True:
        connected, str_dat, client_address = networking.accept(sock_req, logger)
        if connected:
            feat_handler, est_dict = est_eval.get_feat_handler_est_dict(ini, str_dat, args.cat, logger, DB_OP_)
            eval_dict = feat_handler.decode_stream(str_dat, dbg_=False, logger=logger)

            t = threading.Thread(target=est_eval.evaluate, args=(ini, args,
                                                                 feat_handler,
                                                                 str_dat, eval_dict, est_dict,
                                                                 logger,
                                                                 DB_OP_))
            t.start()


if __name__ == "__main__":
    if len(sys.argv) == 1:
        sys.argv.extend([
                         # "--cfg_ini_file", "../sys_temp/cvt_cfg_sa_minds.ini",
                         # "--ml_ini_file", "../sys_temp/cvt_ml_setting.ini",
                         # "--cat", "CVT",

                         # "--cfg_ini_file", "../sys_temp/rh_cfg_sa_posco.ini",
                         # "--ml_ini_file", "../sys_temp/rh_ml_setting.ini",
                         # "--cat", "RH"

                         "--cfg_ini_file", "../sys_component/rh_m6_cfg_sa_minds.ini",
                         "--ml_ini_file", "../sys_component/rh_macro_m6.ini",
                         "--cat", "RH_M6"
                        ])

    main()


