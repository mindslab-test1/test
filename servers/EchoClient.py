#! /usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import socket
import argparse
import configparser
from lib import HoonUtils as utils
from lib import sys_lib as lib


def parse_arguments(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("--cfg_ini_file", required=True, help="configuration ini file")
    return parser.parse_args(argv)


def main(args):

    this_folder = os.path.dirname(os.path.abspath(__file__))
    ini = configparser.ConfigParser()
    ini.read(os.path.join(this_folder, args.cfg_ini_file))
    ini = utils.remove_comments_in_ini(ini, delimiter="###")

    while True:

        ans = input("\n % Type data to send to Echo Server, x to exit : ")
        # ans = "test"
        if ans[0] == 'x':
            print(" # Done")
            sys.exit()

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (ini['SERVER']['ip'], int(ini['SERVER']['port']))

        try:
            print("\n EchoClient # Connecting socket to %s port %s..." % server_address)
            sock.connect(server_address)
        except ConnectionRefusedError:
            print(" EchoClient @ ConnectionRefusedError")
            sys.exit()

        try:
            print(" EchoClient # Sending \"{}\"".format(ans))
            sock.sendall(ans.encode('utf-8'))
        finally:
            print(" EchoClient # Closing socket.")
            sock.close()

        ret_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_address = (ini['CLIENT']['ip'], int(ini['CLIENT']['port']))
        print(" EchoClient # Starting up Client on %s:%s..." % client_address)
        ret_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        ret_sock.bind(client_address)
        ret_sock.listen(5)

        print(" EchoClient # Waiting for a connection...")
        con, server_address = ret_sock.accept()
        print(" EchoClient # Connection for {}...".format(server_address))

        try:
            str_dat = lib.recv_all(con).decode('utf-8')
            print(" EchoClient # Received: \"{}\"".format(str_dat))
        finally:
            con.close()
            ret_sock.close()
            print(" EchoClient # Closing connection and socket")


SELF_TEST_ = True


if __name__ == "__main__":
    if len(sys.argv) == 1:
        if SELF_TEST_:
            sys.argv.extend([
                "--cfg_ini_file", "EchoServer.ini",
            ])
        else:
            sys.argv.extend(["--help"])

    main(parse_arguments(sys.argv[1:]))
