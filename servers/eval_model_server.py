import argparse
import configparser
import socket
import sys

from lib import sys_lib as system_lib
from lib import networking
from models import est_eval


DB_OP_ = True
LOGGER_FOLER = 'log'
SERVER_MODE = 'EVAL'


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--port", required=True, help="port number")
    parser.add_argument("--cat", required=True, help="Category, rh, cvt or rh+")
    parser.add_argument("--ml_ini_file", required=True, help="machine learning ini file")
    parser.add_argument("--networking_ini_file", required=True, help="networking ini file")
    return parser


def main():
    parser = create_parser()
    args = parser.parse_args()

    networking_ini = configparser.ConfigParser()
    networking_ini.read(args.networking_ini_file)
    ml_ini = configparser.ConfigParser()
    ml_ini.read(args.ml_ini_file)
    logger_name = args.cat + '_EVAL'
    logger = system_lib.setup_logger(logger_name,
                                     logger_name.lower(),
                                     folder=LOGGER_FOLER,
                                     console_=True)

    logger.info("START " + args.cat + "_EVAL server")

    port_number = int(args.port)

    sock_req = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('localhost', port_number)
    logger.info("Starting up on %s port %s..." % server_address)
    sock_req.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock_req.bind(server_address)
    sock_req.listen(5)
    while True:
        connected, str_dat, client_address = networking.accept(sock_req, logger)
        if connected:
            feat_handler, est_dict = est_eval.get_feat_handler_est_dict(networking_ini, str_dat, args.cat, logger, DB_OP_, SERVER_MODE)
            eval_dict = feat_handler.decode_stream_comma(str_dat, dbg_=False, logger=logger)
            est_eval.evaluate(networking_ini, args, feat_handler, str_dat, eval_dict, est_dict, logger, DB_OP_)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        sys.argv.extend([
                         "--ml_ini_file", "../sys_component/config_machine_learning/rh_macro_m6.ini",
                         "--networking_ini_file", "../sys_component/config_networking/rh_m6_cfg_sa_minds.ini",
                         "--cat", "RH_M6",
                         "--port", "4064"
                        ])

    main()


