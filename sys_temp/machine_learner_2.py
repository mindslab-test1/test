#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
import sys
from random import randint

import matplotlib
from operator import itemgetter
import matplotlib.pyplot as plt
import time
from lightgbm.sklearn import LGBMRegressor
from xgboost.sklearn import XGBRegressor

from sklearn.ensemble import RandomForestRegressor
# from sklearn.svm import SVR
# from sklearn.svm import NuSVR
from sklearn.multioutput import MultiOutputRegressor
from sklearn.svm import LinearSVR
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import GridSearchCV
import scipy.stats as st
import pickle
import numpy as np
import tensorflow as tf
from lib import posco as posco
from dev_hoon_temp import dataset_handler as Dataset

from tensorflow.python.client import device_lib

__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

SVR_C = 1.0
SVR_epsilon = 0.1


class MachineLearner(object):
    """

    """
    def __init__(self, Feat=None, dataset=None, out_idx=0):
        self.Feat = Feat
        self.dataset = dataset
        self.out_idx = 0
        self.log = ""
        self.est_thresh = 0.0

        self.X_name = None
        self.y_name = None
        self.X_dataset = None
        self.y_dataset = None
        self.y_predict = None
        self.params = None
        self.train_mse = 0
        self.train_mse_2 = 0
        self.test_mse = 0
        self.test_mse_2 = 0
        self.train_acc = 0
        self.train_acc_2 = 0
        self.test_acc = 0
        self.test_acc_2 = 0
        self.acc_thresh = 0
        self.X = [[], []]
        self.y = [[], []]

        self.model = None

    def get_log(self):
        return self.log

    def split_dataset_into_train_and_test(self, dataset_ratio=70):
        """ Split dataset into train and test.

        :param dataset_ratio:
        :return:
        """
        dataset_train, dataset_test = Dataset.split_dataset_into_train_and_test(self.dataset, dataset_ratio, offset=1)
        trans_dataset_train = posco.transpose_list(dataset_train)
        trans_dataset_test  = posco.transpose_list(dataset_test)
        self.X[0] = posco.transpose_list(trans_dataset_train[1:])[1:]
        self.X[1] = posco.transpose_list(trans_dataset_test[1:])[1:]
        self.y[0] = trans_dataset_train[0][1:]
        self.y[1] = trans_dataset_test[0][1:]
        self.X_name = self.dataset[0][1:]
        self.y_name = self.dataset[0][0]


    def split_dataset_into_train_and_test_for_multiple_y(self, dataset_ratio=70, num_y=2):
        """ Split dataset into train and test.
        :param dataset_ratio:
        :return:
        """
        dataset_train, dataset_test = Dataset.split_dataset_into_train_and_test(self.dataset, dataset_ratio, offset=1)
        trans_dataset_train = posco.transpose_list(dataset_train)
        trans_dataset_test = posco.transpose_list(dataset_test)

        self.X[0] = posco.transpose_list(trans_dataset_train[num_y:])[1:]
        self.X[1] = posco.transpose_list(trans_dataset_test[num_y:])[1:]
        self.y[0] = posco.transpose_list(trans_dataset_train[:num_y])[1:]
        self.y[1] = posco.transpose_list(trans_dataset_test[:num_y])[1:]
        self.X_name = self.dataset[0][num_y:]
        self.y_name = self.dataset[0][:num_y]

        #
        self.X_dataset = self.X[0] + self.X[1]
        self.y_dataset = self.y[0] + self.y[1]

        pass


    def encode_symbol_dataset_by_int(self, dataset=None):
        """ Encode symbol dataset by integer

        :param dataset:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = posco.transpose_list(dataset)
        arr = []
        for col in trans_dataset:
            if self.Feat.is_number_class(col[0]):
                try:
                    arr.append([col[0]] + [float(x) for x in col[1:]])
                except (TypeError, ValueError) as e:
                    print(e)
                    print(col[0])
            else:
                try:
                    arr.append([col[0]] + [self.Feat.vars[col[0]].domain.index(x) for x in col[1:]])
                except (TypeError, ValueError) as e:
                    print(e)
                    print(col[0])

        self.dataset = posco.transpose_list(arr)

        return self.dataset

    def encode_symbol_dataset_by_one_hot_encoder(self, dataset=None):
        """ Encode symbol dataset by integer

        :param dataset:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = posco.transpose_list(dataset)
        arr = []
        for col in trans_dataset:
            if self.Feat.is_number_class(col[0]):
                arr.append(col)
            else:
                width = len(self.Feat.vars[col[0]].domain)
                sub_arr = [[]]
                for i in range(width):
                    sub_arr[-1].append("{}__{:d}".format(col[0], i))
                for val in col[1:]:
                    sub_arr.append([0.]*width)
                    try:
                        sub_arr[-1][self.Feat.vars[col[0]].domain.index(val)] = 1.
                    except ValueError as e:
                        pass
                        print(e)
                trans_sub_arr = posco.transpose_list(sub_arr)
                for i in range(width):
                    arr.append(trans_sub_arr[i])
        self.dataset = posco.transpose_list(arr)

        return self.dataset

    def normalize_number_features(self, dataset=None, min_val=-1, max_val=1, offset=0):
        """ Normalize number features by pre-defined range

        :param dataset:
        :param min_val:
        :param max_val:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = posco.transpose_list(dataset)

        # arr = [[trans_dataset[0][0]] + [float(x) for x in trans_dataset[0][1:]]]
        arr = []
        for col in trans_dataset[offset:]:
            if self.Feat.is_number_class(col[0]):
                arr.append([col[0]])
                ptr = self.Feat.vars[col[0]]
                val_range = float(ptr.max_thresh - ptr.min_thresh)
                if val_range == 0:
                    print(col[0])
                    for val in col[1:]:
                        arr[-1].append(float(val))
                else:
                    for val in col[1:]:
                        # print(val)

                        try:
                            val = float(val)
                        except ValueError:
                            val = 0
                        if val < ptr.min_thresh:
                            val = ptr.min_thresh
                        elif val > ptr.max_thresh:
                            val = ptr.max_thresh
                        # if val_range == 0:
                        #    arr[-1].append(col[1:])
                        # else:
                        arr[-1].append((val - ptr.min_thresh) / val_range * (max_val - min_val) + min_val)
            else:
                arr.append(col)

        self.dataset = posco.transpose_list(arr)

        return self.dataset

    def denormalize_output(self, predicted_output, real_max, real_min, norm_min=-1, norm_max=1):
        return (predicted_output - norm_min) / (norm_max - norm_min) * (real_max - real_min) + real_min

    def check_est_accuracy(self, error_arr, est_thresh=None):
        """ Check estimation accuracy based on estimation threshold.

        :param error_arr:
        :param est_thresh:
        :return:
        """
        if not est_thresh:
            est_thresh = self.est_thresh

        acc_arr = [x < est_thresh for x in error_arr]

        if len(acc_arr) == 0:
            return 99.99
        else:
            return 100. * sum(acc_arr) / float(len(acc_arr))

    def gen_plot(self, xmin, xmax, margin, train_val, test_val, train_pred_val, test_pred_val, train_acc, test_acc, c_name, plot_name,
                 method):

        xmin = xmin
        xmax = xmax

        under = []
        under.append((xmin - 30) - margin)
        under.append((xmax + 30) - margin)
        ideal = []
        ideal.append(xmin - 30)
        ideal.append(xmax + 30)
        below = []
        below.append((xmin - 30) + margin)
        below.append((xmax + 30) + margin)

        fig = plt.figure(1)
        fig = plt.figure(figsize=(25, 8))
        plt.subplot(1, 2, 1)
        plt.plot(ideal, under, color='r', linestyle='--', label='{:3d}°C margin'.format(int(margin)))
        plt.plot(ideal, below, color='r', linestyle='--')
        plt.xlim(xmin, xmax)
        plt.ylim(xmin, xmax)
        mod_train_y = np.array([np.random.uniform(-0.5, 0.5) for i in range(len(train_val))]) + train_val
        mod_predict_y = np.array([np.random.uniform(-0.5, 0.5) for i in range(len(train_pred_val))]) + train_pred_val
        plt.plot(ideal, ideal, color='r', linewidth=1, label='ideal value')
        plt.scatter(mod_train_y, mod_predict_y, color='g', s=0.5, label='real value')
        plt.xlabel('Real value of {}'.format(c_name))
        plt.ylabel('Prediction value of {}'.format(c_name))
        plt.title('Training results of {}    {} Distribution Graph'.format(method, c_name))
        plt.legend()

        plt.subplot(1, 2, 2)
        plt.plot(ideal, under, color='r', linestyle='--', label='{:3d}°C margin'.format(int(margin)))
        plt.plot(ideal, below, color='r', linestyle='--')
        plt.xlim(xmin, xmax)
        plt.ylim(xmin, xmax)
        mod_test_y = np.array([np.random.uniform(-0.5, 0.5) for i in range(len(test_val))]) + test_val
        mod_predict_test_y = np.array(
            [np.random.uniform(-0.5, 0.5) for i in range(len(test_pred_val))]) + test_pred_val
        plt.plot(ideal, ideal, color='r', linewidth=1, label='ideal value')
        plt.scatter(mod_test_y, mod_predict_test_y, color='g', s=0.5, label='real value')
        plt.xlabel('Real value of {} (°C)'.format(c_name))
        plt.ylabel('Prediction value of {} (°C)'.format(c_name))
        plt.title('Test results of {}    {} Distribution Graph'.format(method,  c_name))
        plt.legend()

        fig_name = c_name + '_' + plot_name
        fig.savefig(fig_name)

        pass

    # def run_random_forest_regressor(self,
    #                                 dataset_ratio=70,
    #                                 n_estimators=None,
    #                                 est_thresh=7
    #                                 ):
    #     """ Run random forest regressor with various environments.
    #
    #     :param dataset_ratio:
    #     :param n_estimators:
    #     :param est_thresh:
    #     :return:
    #     """
    #     msg = "\n # Random Forest Regression"
    #
    #     if not n_estimators:
    #         n_estimators = [10]
    #
    #     self.split_dataset_into_train_and_test(dataset_ratio=dataset_ratio)
    #
    #     rst_arr = []
    #     for n_estimator in n_estimators:
    #         rst_arr.append([n_estimator])
    #         mdl = RandomForestRegressor(n_estimators=n_estimator)
    #         mdl.fit(self.X[0], self.y[0])
    #         self.RFR_num_estimators = n_estimator
    #         self.RFR_model = mdl
    #         pred_y  = []
    #         est_err = []
    #         est_acc = []
    #         mape_arr = []
    #         rmse_arr = []
    #         sigma = [0, 0]
    #         # sigma = [3,2]   # 96.52 / 92.09
    #         # sigma = [3.7, 2.1]  # 93.64 / 88.96
    #         # sigma = [4,3]   # 90.37 / 87.37
    #         # sigma = [5,4]   # 81.80 / 81.38
    #         # sigma = [6,5]   # 74.38 / 74.91
    #         # sigma = [3.5, 2.5]  # 93.97 / 90.23
    #         sigma = [5, 4.5]
    #
    #         for i in range(2):
    #             pred_y.append(mdl.predict(self.X[i]))
    #             # pred_y.append(mdl.predict(self.X[i]) + np.random.normal(0,sigma[i],len(self.X[i])))
    #             est_err.append(abs(pred_y[i] - np.array(self.y[i])))
    #             est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
    #             mape_arr.append(posco.calc_MAPE(self.y[i], pred_y[-1]))
    #             rmse_arr.append(posco.calc_RMSE(self.y[i], pred_y[-1]))
    #             rst_arr[-1].append(est_err[-1])
    #         feat_impt = [[self.dataset[0][idx][1:], mdl.feature_importances_[idx]] for idx in range(len(self.dataset[0][1:]))]
    #         feat_impt = sorted(feat_impt, key=itemgetter(1), reverse=True)
    #         rst_arr[-1].append(feat_impt)
    #         rst_arr[-1].insert(1, est_acc[0])
    #         rst_arr[-1].insert(2, est_acc[1])
    #
    #         print(" # RFR processing with n_estimation = {:d}".format(n_estimator))
    #         msg += "\n\n # n_estimator : {:d}".format(n_estimator)
    #         msg += "\n   |          |  train # |   test #  |  train % |  test %  |"
    #         msg += "\n   | Overall  |  {:6d}  |   {:6d}  |".format(est_err[0].shape[0], est_err[1].shape[0])
    #         msg +=                  "  {:6.2f}  |  {:6.2f}  |".format(self.check_est_accuracy(est_err[0], est_thresh),
    #                                                                   self.check_est_accuracy(est_err[1], est_thresh))
    #         if False:   # This is a routine to draw the plot to show the accuracy performance according to estimators.
    #             n_estimators = [x[0] for x in rst_arr]
    #             train_acc = [x[1] for x in rst_arr]
    #             test_acc = [x[2] for x in rst_arr]
    #             plt.plot(n_estimators, train_acc, 'b', label='Train')
    #             plt.plot(n_estimators, test_acc, 'r', label='Test')
    #             plt.ylim(50,110)
    #             plt.xticks(n_estimators)
    #             plt.yticks(list(np.arange(50, 105, 5)))
    #             plt.legend()
    #             plt.xlabel("Number of tree in the forest")
    #             plt.ylabel("Estimation accuracy with 7 degree")
    #             plt.title("Performance variation according to the number of tree")
    #             plt.savefig("RFR_performance_variation.png")
    #
    #         if False:
    #             bar_name = []
    #             cnt = 0
    #             for content in feat_impt:
    #                 plt.barh(cnt, content[1])
    #                 bar_name.append(content[0])
    #                 cnt += 1
    #                 if cnt > 10:
    #                     break
    #             plt.xlim(0, feat_impt[0][1]*1.2)
    #             plt.xlabel('Feature importance')
    #             plt.ylabel('Features')
    #             plt.yticks([i for i, _ in enumerate(bar_name)], bar_name)
    #             plt.title('Random Forest Classifier')
    #             matplotlib.rcParams.update({'font.size': 22})
    #             plt.savefig('RFR_feat_impt.png')
    #
    #         if False:
    #             trans_train_X = posco.transpose_list(self.X[0])
    #             trans_test_X = posco.transpose_list(self.X[1])
    #             for k in range(len(self.X_name)):
    #                 key = self.X_name[k]
    #                 if True:
    #                 #if not self.Feat.is_number_class(key):
    #                     domain_len = len(self.Feat.vars[key].domain)
    #                     msg += "\n # analyzing estimation performance based on {} ({:d})".format(key, domain_len)
    #                     print("\n # analyzing estimation performance based on {} ({:d})".format(key, domain_len))
    #                     train_cnt = []
    #                     train_err = []
    #                     train_acc = []
    #                     test_cnt = []
    #                     test_err = []
    #                     test_acc = []
    #                     for i in range(domain_len):
    #                         train_cnt.append(0)
    #                         train_err.append([])
    #                         train_acc.append(0)
    #                         for j in range(len(self.X[0])):
    #                             if int(trans_train_X[k][j]) == int(i):
    #                                 train_cnt[-1] += 1
    #                                 train_err[-1].append(est_err[0][j])
    #                         train_acc[-1] = self.check_est_accuracy(train_err[-1], est_thresh)
    #                         test_cnt.append(0)
    #                         test_err.append([])
    #                         test_acc.append(0)
    #                         for j in range(len(self.X[1])):
    #                             if int(trans_test_X[k][j]) == int(i):
    #                                 test_cnt[-1] += 1
    #                                 test_err[-1].append(est_err[1][j])
    #                         test_acc[-1] = self.check_est_accuracy(test_err[-1], est_thresh)
    #                         msg += "\n   | {:>8} ".format(self.Feat.vars[key].domain[i])
    #                         msg +=      "|  {:6d}  |  {:6d}  ".format(train_cnt[-1], test_cnt[-1])
    #                         msg +=      "|  {:6.2f}  |   {:6.2f}  |".format(train_acc[-1], test_acc[-1])
    #                     msg += "\n   |          |  {:6d}  |  {:6d}  |".format(sum(train_cnt), sum(test_cnt))
    #
    #                     if False:
    #                         x_range = list(np.arange(domain_len))
    #                         plt.plot(x_range, train_acc, 'b', label='Train')
    #                         plt.plot(x_range, test_acc, 'r', label='Test')
    #                         plt.ylim(80,110)
    #                         plt.xticks(x_range)
    #                         plt.yticks(list(np.arange(80, 101, 1)))
    #                         plt.legend()
    #                         plt.xlabel("Symbol index")
    #                         plt.ylabel("Estimation accuracy with 7 degree")
    #                         plt.title("Performance variation according to symbol in {}".format(key))
    #                         plt.tight_layout()
    #                         plt.savefig("perf_variation_{}.png".format(key))
    #
    #     self.log = msg
    #     return

    def random_forest_regressor(self, rfr_cfg):
        """ Run random forest regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        msg = "\n # Random Forest Regression"
        n_estimators = int(rfr_cfg['n_estimators'])
        min_samples_leaf = int(rfr_cfg['min_samples_leaf'])

        if rfr_cfg['max_depth'] == "none":
            max_depth = None
        else:
            max_depth = int(rfr_cfg['max_depth'])

        if rfr_cfg['max_leaf_nodes'] == "none":
            max_leaf_nodes = None
        else:
            max_leaf_nodes = int(rfr_cfg['max_leaf_nodes'])

        max_features = rfr_cfg['max_features']
        if max_features != "auto" and max_features != "sqrt" and max_features != "log2" :
            max_features = int(max_features)
        # if rfr_cfg['max_features'] == "num_feat":
        #     max_features = len(self.X[0])
        # elif rfr_cfg['max_features'] == "sqrt":
        #     max_features = math.sqrt(len(self.X[0]))
        # elif rfr_cfg['max_features'] == "log2":
        #     max_features = math.log2(len(self.X[0]))
        # else:
        #     max_features = int(rfr_cfg['max_features'])

        est_thresh = float(rfr_cfg['est_thresh'])
        self.acc_thresh = est_thresh
        # self.split_dataset_into_train_and_test(dataset_ratio=int(rfr_cfg['train_ratio']))


        mdl = RandomForestRegressor(n_estimators=n_estimators, min_samples_leaf=min_samples_leaf,
                                    max_depth=max_depth, max_leaf_nodes=max_leaf_nodes,
                                    max_features=max_features)
        mdl.fit(self.X[0], self.y[0])
        self.model = mdl
        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []
        # sigma = [0, 0]
        # sigma = [3,2]   # 96.52 / 92.09
        # sigma = [3.7, 2.1]  # 93.64 / 88.96
        # sigma = [4,3]   # 90.37 / 87.37
        # sigma = [5,4]   # 81.80 / 81.38
        # sigma = [6,5]   # 74.38 / 74.91
        # sigma = [3.5, 2.5]  # 93.97 / 90.23
        # sigma = [5, 4.5]

        for i in range(2):
            pred_y.append(mdl.predict(self.X[i]))
            # pred_y.append(mdl.predict(self.X[i]) + np.random.normal(0,sigma[i],len(self.X[i])))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
            mape_arr.append(posco.calc_MAPE(self.y[i], pred_y[-1]))
            rmse_arr.append(posco.calc_RMSE(self.y[i], pred_y[-1]))
        # feat_impt = [[self.dataset[0][idx][1:], mdl.feature_importances_[idx]] for idx in
        #              range(len(self.dataset[0][1:]))]
        # feat_impt = sorted(feat_impt, key=itemgetter(1), reverse=True)
        self.y_predict = pred_y

        self.train_acc = self.check_est_accuracy(est_err[0], est_thresh)
        self.test_acc = self.check_est_accuracy(est_err[1], est_thresh)
        self.train_mse = mean_squared_error(self.y[0], pred_y[0].tolist())
        self.test_mse = mean_squared_error(self.y[1], pred_y[1].tolist())

        # filename = "result_" + rfr_cfg['train_ratio'] + "_" + \
        #            rfr_cfg['n_estimators'] + "_" + rfr_cfg['min_samples_leaf'] + "_" + \
        #            rfr_cfg['max_depth'] + "_" + rfr_cfg['max_leaf_nodes'] + "_" + \
        #            rfr_cfg['max_features'] + "_" + rfr_cfg['est_thresh'] + rfr_cfg['filename_postfix']
        # with open(rfr_cfg['result_dir'] + filename, 'a') as f:
        #     f.write("{:6.2f},{:6.2f},{:6.3f},{:6.3f}\n".format(
        #                 self.check_est_accuracy(est_err[0], est_thresh),
        #                 self.check_est_accuracy(est_err[1], est_thresh),
        #                 mape_arr[1],
        #                 rmse_arr[1]
        #             ))
        msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        msg += "\n # train mse : {:4.2f}, test mse : {:4.2f}".format(self.train_mse, self.test_mse)
        self.log = msg

        #trans_dataset = posco.transpose_list(self.dataset[1:])
        #self.X_dataset = posco.transpose_list(trans_dataset[1:])
        #self.y_dataset = trans_dataset[0]

        self.params = rfr_cfg

        if False:
             self.gen_plot(xmin=0, xmax=150, margin=est_thresh,
                           train_val=self.y[0], test_val=self.y[1],
                           train_pred_val=pred_y[0], test_pred_val=pred_y[1],
                           train_acc=self.train_acc, test_acc=self.test_acc,
                           c_name='CVT Temperature DROP', plot_name='Data Distribution Graph', method='RFR')

        return

        #
        #
        # num_train = len(self.train_X)
        # num_test  = len(self.test_X)
        # num_ratio = int(100. * num_train / float(num_train + num_test))
        # msg  = "\n"
        # msg += "\n # Machine learning by random forest regressor"
        # msg += "\n   > Train vs test = {:d} vs {:d} ({:d}%)".format(num_train, num_test, num_ratio)
        # msg += "\n   > Train dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_train_abs.mean(),
        #                                                                                err_train_abs.var())
        # msg += "\n   > Test  dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_train_abs.mean(),
        #                                                                                err_train_abs.var())
        # msg += "\n   > Estimator accuracy for train dataset = {:6.2f}".format(train_accuracy)
        # msg += "\n   > Estimator accuracy for test  dataset = {:6.2f}".format(test_accuracy)
        # msg += "\n   > Sorted feature importance"
        # for content in sorted_feat_impt:
        #     msg += "\n   > {:<30} : {:6d}".format(content[0], int(content[1] * 100000))
        # self.log = msg
        #
        # max_range = max(max(err_train_abs), max(err_test_abs), est_margin) + 1
        # fig = plt.figure()
        # plt.subplot(2,1,1)
        # plt.plot(err_train_abs, 'b', [est_margin,] * len(err_train_abs), 'r')
        # plt.ylim(0, max_range)
        # plt.title('Training ABS error({:2d}%)'.format(int(train_accuracy)))
        # plt.subplot(2,1,2)
        # plt.plot(err_test_abs, 'b', [est_margin,] * len(err_test_abs), 'r')
        # plt.ylim(0, max_range)
        # plt.title('Test ABS error({:2d}%)'.format(int(test_accuracy)))
        # fig.tight_layout()
        # if plot_filename_prefix:
        #     fig.savefig(plot_filename_prefix + 'time.png')
        # if plot_:
        #     plt.show()
        # plt.close(fig)
        #
        # fig = plt.figure()
        # plt.subplot(2,1,1)
        # y, _, _ = plt.hist(err_train, 256, facecolor='blue')
        # axes = plt.gca()
        # axes.set_xlim([-est_margin*2,est_margin*2])
        # axes.set_ylim([0, y.max()])
        # plt.title('Histogram of training error')
        # plt.subplot(2,1,2)
        # plt.hist(err_test,  256, facecolor='blue')
        # axes = plt.gca()
        # axes.set_xlim([-est_margin*2,est_margin*2])
        # axes.set_ylim([0, y.max()])
        # plt.title('Histogram of test error')
        # fig.tight_layout()
        # if plot_filename_prefix:
        #     fig.savefig(plot_filename_prefix + 'hist.png')
        # if plot_:
        #     plt.show()
        # plt.close(fig)
        #
        # est_thresholds = np.arange(0, est_margin*2, 0.1)
        # train_acc = []
        # test_acc = []
        # for thresh in est_thresholds:
        #     train_acc.append(100. * sum(err_train_abs <= thresh) / err_train_abs.shape[0])
        #     test_acc.append(100. * sum(err_test_abs  <= thresh) / err_test_abs.shape[0])
        # fig = plt.figure()
        # plt.plot(est_thresholds, train_acc, 'b-', label='train')
        # plt.plot(est_thresholds, test_acc,  'r-', label='test')
        # plt.title('Performance variation according to tolerance')
        # plt.grid(True)
        # plt.legend(loc='lower right', shadow=True)
        # fig.tight_layout()
        # if plot_filename_prefix:
        #     fig.savefig(plot_filename_prefix + 'perf.png')
        # if plot_:
        #     plt.show()
        # plt.close(fig)
        #
        # if pkl_filename:
        #     pickle.dump([mdl, self.name_X, self.name_y, msg, feat_mdl], open(pkl_filename, 'wb'))
        #
        # pass

    def random_forest_regressor_for_multiple_y(self, rfr_cfg):
        """ Run random forest regressor with various environments.
        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        msg = "\n # Random Forest Regression"
        n_estimators = int(rfr_cfg['n_estimators'])
        min_samples_leaf = int(rfr_cfg['min_samples_leaf'])

        if rfr_cfg['max_depth'] == "none":
            max_depth = None
        else:
            max_depth = int(rfr_cfg['max_depth'])

        if rfr_cfg['max_leaf_nodes'] == "none":
            max_leaf_nodes = None
        else:
            max_leaf_nodes = int(rfr_cfg['max_leaf_nodes'])

        max_features = rfr_cfg['max_features']
        if max_features != "auto" and max_features != "sqrt" and max_features != "log2":
            max_features = int(max_features)

        time_est_thresh = float(rfr_cfg['time_est_thresh'])
        temp_est_thresh = float(rfr_cfg['temp_est_thresh'])

        # self.split_dataset_into_train_and_test(dataset_ratio=int(rfr_cfg['train_ratio']))


        mdl = MultiOutputRegressor(RandomForestRegressor(n_estimators=n_estimators, min_samples_leaf=min_samples_leaf,
                                                         max_depth=max_depth, max_leaf_nodes=max_leaf_nodes,
                                                         max_features=max_features))
        mdl.fit(self.X[0], self.y[0])
        self.RFR_num_estimators = n_estimators
        self.model = mdl
        pred_y = []

        est_err_time = []
        est_acc_time = []
        mse_arr_time = []

        est_err_temp = []
        est_acc_temp = []
        mse_arr_temp = []

        for i in range(2):  # Train & Test
            pred_y.append(mdl.predict(self.X[i]))
            # pred_y.append(mdl.predict(self.X[i]) + np.random.normal(0,sigma[i],len(self.X[i])))
            est_err_time.append(abs(np.array(pred_y[i])[:, 0] - np.array(self.y[i])[:, 0]))
            est_acc_time.append(self.check_est_accuracy(est_err_time[-1], time_est_thresh))
            mse_arr_time.append(mean_squared_error(np.array(self.y[i])[:, 0], np.array(pred_y[-1])[:, 0]))

            est_err_temp.append(abs(np.array(pred_y[i])[:, 1] - np.array(self.y[i])[:, 1]))
            est_acc_temp.append(self.check_est_accuracy(est_err_temp[-1], temp_est_thresh))
            mse_arr_temp.append(mean_squared_error(np.array(self.y[i])[:, 1], np.array(pred_y[-1])[:, 1]))

        self.train_acc, self.test_acc = est_acc_time
        self.train_acc_2, self.test_acc_2 = est_acc_temp
        self.train_mse, self.test_mse = mse_arr_time
        self.train_mse_2, self.test_mse_2 = mse_arr_temp
        self.log = msg

        print(msg)
        return

    def support_vector_regressor(self, est_margin=sys.float_info.epsilon, plot_filename_prefix='', plot_=True, pkl_filename=''):
        # reg = SVR(C=SVR_C, epsilon=SVR_epsilon)
        reg = LinearSVR()
        reg.fit(self.train_X, self.train_y)
        train_y_predict = reg.predict(self.train_X)
        test_y_predict = reg.predict(self.test_X)
        err_train = abs(train_y_predict - self.train_y)
        err_test  = abs(test_y_predict  - self.test_y)
        train_result = err_train <= est_margin
        test_result  = err_test  <= est_margin
        train_accuracy = 100. * sum(train_result) / float(len(train_result))
        test_accuracy  = 100. * sum(test_result)  / float(len(test_result))

        num_train = len(self.train_X)
        num_test  = len(self.test_X)
        num_ratio = int(100. * num_train / float(num_train + num_test))
        msg  = "\n"
        msg += "\n # Machine learning by support vector regressor"
        msg += "\n   > Train vs test = {:d} vs {:d} ({:d}%)".format(num_train, num_test, num_ratio)
        msg += "\n   > Train dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_train.mean(), err_train.var())
        msg += "\n   > Test  dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_test.mean(),  err_test.var())
        msg += "\n   > Estimator accuracy for train dataset = {:6.2f}".format(train_accuracy)
        msg += "\n   > Estimator accuracy for test  dataset = {:6.2f}".format(test_accuracy)
        self.log = msg
        print(msg)
        print(" real  = ({:f} {:f}) -> ({:f} {:f})".format(min(self.train_y), max(self.train_y),
                                                           min(train_y_predict), max(train_y_predict)))
        print(" real  = ({:f} {:f}) -> ({:f} {:f})".format(min(self.test_y), max(self.test_y),
                                                           min(test_y_predict), max(test_y_predict)))

        max_range = max(max(err_train), max(err_test), est_margin) + 1
        fig = plt.figure()
        plt.subplot(1,2,1)
        plt.plot(err_train, 'b', [est_margin,] * len(err_train), 'r')
        plt.ylim(0, max_range)
        plt.title('Training ABS error({:2d}%)'.format(int(train_accuracy)))
        plt.subplot(1,2,2)
        plt.plot(err_test, 'b', [est_margin,] * len(err_test), 'r')
        plt.ylim(0, max_range)
        plt.title('Test ABS error({:2d}%)'.format(int(test_accuracy)))
        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix)
        if plot_:
            plt.show()
        else:
            plt.close(fig)

        if pkl_filename:
            pickle.dump([reg, self.name_X, self.name_y, msg], open(pkl_filename, 'wb'))
        pass

    # def deep_neuralnet_regressor(self, dataset_ratio=70, est_margin=sys.float_info.epsilon, plot_filename_prefix='', plot_=True, pkl_filename=''):
    #
    #     self.split_dataset_into_train_and_test(dataset_ratio=dataset_ratio)
    #
    #     trans_train_X = posco.transpose_list(self.X[0])
    #     trans_test_X = posco.transpose_list(self.X[1])
    #
    #     train_X_n = []
    #     test_X_n = []
    #     dat_min = []
    #     dat_max = []
    #
    #     for idx in range(len(trans_train_X)):
    #         train_X_n.append(MinMaxScaler(trans_train_X[idx]))
    #         test_X_n.append(MinMaxScaler(trans_test_X[idx]))
    #         dat_min.append(min(trans_train_X[idx]))
    #         dat_max.append(max(trans_train_X[idx]))
    #
    #     output_min = min(self.y[0])
    #     output_max = max(self.y[0])
    #
    #     train_X_n = posco.transpose_list(train_X_n)
    #     test_X_n = posco.transpose_list(test_X_n)
    #     train_y_n = MinMaxScaler(self.y[0])
    #     test_y_n = MinMaxScaler(self.y[1])
    #
    #     train_X_n = np.array(train_X_n)
    #     test_X_n = np.array(test_X_n)
    #     train_y_n = np.array(train_y_n)
    #     test_y_n = np.array(test_y_n)
    #     train_y_n.shape = (train_y_n.shape[0], 1)
    #     test_y_n.shape = (test_y_n.shape[0], 1)
    #
    #     def get_train_data():
    #         X_train_tf = tf.constant(train_X_n)
    #         Y_train_tf = tf.constant(train_y_n)
    #         return X_train_tf, Y_train_tf
    #
    #     nsteps = 15001
    #
    #     dnn_num_feat = train_X_n.shape[1]
    #     dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=dnn_num_feat)]
    #     dnn_active_fn = tf.tanh
    #     dnn_unit = [2]
    #     dnn_learn_rate = 0.2
    #     dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)
    #
    #     dnn_dir = "dnn_model"
    #
    #     msg = "\n"
    #     msg += "------------------------------------\n"
    #
    #     reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col, activation_fn=dnn_active_fn,
    #                                         hidden_units=dnn_unit, model_dir=dnn_dir,
    #                                         optimizer=dnn_optimizer)
    #
    #
    #     reg.fit(input_fn=get_train_data, steps=nsteps)
    #     train_y_predict_n = reg.predict(train_X_n)
    #     train_y_predict_n = np.array(list(train_y_predict_n))
    #     test_y_predict_n = reg.predict(test_X_n)
    #     test_y_predict_n = np.array(list(test_y_predict_n))
    #     train_y_predict = NormToReal(self.y[0], train_y_predict_n)
    #     test_y_predict = NormToReal(self.y[0], test_y_predict_n)
    #     err_train = abs(train_y_predict - self.y[0])
    #     err_test = abs(test_y_predict - self.y[1])
    #     train_result = err_train <= est_margin
    #     test_result = err_test <= est_margin
    #     train_accuracy = 100. * sum(train_result) / float(len(train_result))
    #     test_accuracy = 100. * sum(test_result) / float(len(test_result))
    #
    #     num_train = len(self.X[0])
    #     num_test = len(self.X[1])
    #     num_ratio = int(100. * num_train / float(num_train + num_test))
    #
    #     msg += "{:6.2f}".format(train_accuracy)
    #     msg += "{:6.2f}".format(test_accuracy)
    #
    #     print(msg)
    #     self.log = msg
        #
        # max_range = max(max(err_train), max(err_test), est_margin) + 1
        # fig = plt.figure()
        # plt.subplot(1, 2, 1)
        # plt.plot(err_train, 'b', [est_margin, ] * len(err_train), 'r')
        # plt.ylim(0, max_range)
        # plt.title('Training ABS error({:2d}%)'.format(int(train_accuracy)))
        # plt.subplot(1, 2, 2)
        # plt.plot(err_test, 'b', [est_margin, ] * len(err_test), 'r')
        # plt.ylim(0, max_range)
        # plt.title('Test ABS error({:2d}%)'.format(int(test_accuracy)))
        #
        # dat_minmax = []
        # dat_minmax.append(self.X_name)
        # dat_minmax.append(dat_min)
        # dat_minmax.append(dat_max)
        #
        # if plot_filename_prefix:
        #     fig.savefig(plot_filename_prefix)
        # if plot_:
        #     plt.show()
        # else:
        #     plt.close(fig)
        #
        # if pkl_filename:
        #     pickle.dump([self.X_name, self.y_name, msg, dat_minmax, output_min, output_max, dnn_learn_rate],
        #                 open(pkl_filename, 'wb'))

    def deep_neuralnet_regressor(self, dnn_cfg):

        train_X_n = np.array(self.X[0])
        test_X_n = np.array(self.X[1])
        train_y_n = np.array(self.y[0])
        test_y_n = np.array(self.y[1])

        train_y_n.shape = (train_y_n.shape[0], 1)
        test_y_n.shape = (test_y_n.shape[0], 1)

        def get_train_data():
            X_train_tf = tf.constant(train_X_n)
            Y_train_tf = tf.constant(train_y_n)
            return X_train_tf, Y_train_tf

        nsteps = int(dnn_cfg['nsteps'])

        dnn_num_feat = train_X_n.shape[1]
        dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=dnn_num_feat)]

        if dnn_cfg['active_fn'] == "relu":
            dnn_active_fn = tf.nn.relu
        elif dnn_cfg['active_fn'] == "sigmoid":
            dnn_active_fn = tf.sigmoid
        else:
            dnn_active_fn = tf.tanh
        dnn_unit = dnn_cfg['unit'].split(',')
        dnn_unit = [int(u) for u in dnn_unit]
        dnn_learn_rate = float(dnn_cfg['learn_rate'])
        if dnn_cfg['optimizer'] == "adadelta":
            dnn_optimizer = tf.train.AdadeltaOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "adam":
            dnn_optimizer = tf.train.AdamOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "ftrl":
            dnn_optimizer = tf.train.FtrlOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "gd":
            dnn_optimizer = tf.train.GradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "proximalGrad":
            dnn_optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "proximalAdagrad":
            dnn_optimizer = tf.train.ProximalAdagradOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "RMSProp":
            dnn_optimizer = tf.train.RMSPropOptimizer(learning_rate=dnn_learn_rate)
        else:
            dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)

        dnn_dropout = None
        if dnn_cfg['dropout'] != 'None':
            dnn_dropout = float(dnn_cfg['dropout'])

        msg = "\n"
        msg += "------------------------------------\n"


        reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col, activation_fn=dnn_active_fn,
                                            hidden_units=dnn_unit, model_dir=dnn_cfg['model_dir'],
                                            optimizer=dnn_optimizer, dropout=dnn_dropout)

        reg.fit(input_fn=get_train_data, steps=nsteps)
        self.model = reg
        train_y_predict_n = reg.predict(train_X_n)
        train_y_predict_n = np.array(list(train_y_predict_n))
        test_y_predict_n = reg.predict(test_X_n)
        test_y_predict_n = np.array(list(test_y_predict_n))

        train_y_predict = self.denormalize_output(train_y_predict_n, self.Feat.vars[self.y_name].max_thresh,
                                                  self.Feat.vars[self.y_name].min_thresh)
        test_y_predict = self.denormalize_output(test_y_predict_n, self.Feat.vars[self.y_name].max_thresh,
                                                 self.Feat.vars[self.y_name].min_thresh)

        real_y_0, real_y_1 = self.denormalize_y()

        err_train = abs(train_y_predict - real_y_0)
        err_test = abs(test_y_predict - real_y_1)
        self.train_mse = mean_squared_error(real_y_0, train_y_predict)
        self.test_mse = mean_squared_error(real_y_1, test_y_predict)

        train_result = err_train <= float(dnn_cfg['est_margin'])
        test_result = err_test <= float(dnn_cfg['est_margin'])

        train_accuracy = 100. * sum(train_result) / float(len(train_result))
        test_accuracy = 100. * sum(test_result) / float(len(test_result))

        self.train_acc = train_accuracy
        self.test_acc = test_accuracy

        num_train = len(self.X[0])
        num_test = len(self.X[1])
        num_ratio = int(100. * num_train / float(num_train + num_test))

        msg += "{:6.2f}".format(self.train_acc)
        msg += "{:6.2f}".format(self.test_acc)

        # filename = "result_" + dnn_cfg['train_ratio'] + "_" +\
        #            dnn_cfg['nsteps'] + "_" + dnn_cfg['unit'] + "_" +\
        #            dnn_cfg['learn_rate'] + "_" + dnn_cfg['dropout'] + "_" +\
        #            str(dnn_num_feat) + dnn_cfg['filename_postfix']
        #
        # with open(dnn_cfg['result_dir'] + filename, 'a') as f:
        #     f.write("{:6.2f},{:6.2f},{:6.2f},{:6.2f}\n".
        #             format(train_accuracy,
        #                    test_accuracy,
        #                    mean_squared_err_train,
        #                    mean_squared_err_test
        #             ))
        print(msg)
        self.log = msg

        trans_dataset = posco.transpose_list(self.dataset[1:])
        self.X_dataset = posco.transpose_list(trans_dataset[1:])
        self.y_dataset = trans_dataset[0]

        self.params = dnn_cfg

        # dat_minmax = []
        # dat_minmax.append(self.X_name)
        # dat_minmax.append(dat_min)
        # dat_minmax.append(dat_max)

    def deep_neuralnet_regressor_for_multiple_y(self, dnn_cfg, num_y=2):

        train_X_n = np.array(self.X[0])
        test_X_n = np.array(self.X[1])
        train_y_n = np.array(self.y[0])
        test_y_n = np.array(self.y[1])

        train_y_n.shape = (train_y_n.shape[0], num_y)
        test_y_n.shape = (test_y_n.shape[0], num_y)

        def get_train_data():
            X_train_tf = tf.constant(train_X_n)
            Y_train_tf = tf.constant(train_y_n)
            return X_train_tf, Y_train_tf

        nsteps = int(dnn_cfg['nsteps'])

        dnn_num_feat = train_X_n.shape[1]
        dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=dnn_num_feat)]

        if dnn_cfg['active_fn'] == "relu":
            dnn_active_fn = tf.nn.relu
        elif dnn_cfg['active_fn'] == "sigmoid":
            dnn_active_fn = tf.sigmoid
        else:
            dnn_active_fn = tf.tanh
        dnn_unit = dnn_cfg['unit'].split(',')
        dnn_unit = [int(u) for u in dnn_unit]
        dnn_learn_rate = float(dnn_cfg['learn_rate'])
        if dnn_cfg['optimizer'] == "adadelta":
            dnn_optimizer = tf.train.AdadeltaOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "adam":
            dnn_optimizer = tf.train.AdamOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "ftrl":
            dnn_optimizer = tf.train.FtrlOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "gd":
            dnn_optimizer = tf.train.GradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "proximalGrad":
            dnn_optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "proximalAdagrad":
            dnn_optimizer = tf.train.ProximalAdagradOptimizer(learning_rate=dnn_learn_rate)
        elif dnn_cfg['optimizer'] == "RMSProp":
            dnn_optimizer = tf.train.RMSPropOptimizer(learning_rate=dnn_learn_rate)
        else:
            dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)

        dnn_dropout = None
        if dnn_cfg['dropout'] != 'None':
            dnn_dropout = float(dnn_cfg['dropout'])

        msg = "\n"
        msg += "------------------------------------\n"
        # config_proto = tf.ConfigProto(allow_soft_placement=True, log_device_placement=True)
        # config_proto.gpu_options.allow_growth = True

        reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col, activation_fn=dnn_active_fn,
                                            hidden_units=dnn_unit, model_dir=dnn_cfg['model_dir'],
                                            optimizer=dnn_optimizer, dropout=dnn_dropout,
                                            label_dimension=2)
        # config=tf.contrib.learn.RunConfig(session_config=config_proto))

        reg.fit(input_fn=get_train_data, steps=nsteps)
        train_y_predict_n = reg.predict(train_X_n)
        train_y_predict_n = np.array(list(train_y_predict_n))
        test_y_predict_n = reg.predict(test_X_n)
        test_y_predict_n = np.array(list(test_y_predict_n))

        train_y_predict_time = self.denormalize_output(train_y_predict_n[:, 0],
                                                       self.Feat.vars[self.y_name[0]].max_thresh, self.Feat.vars[
                                                           self.y_name[0]].min_thresh)
        train_y_predict_temp = self.denormalize_output(train_y_predict_n[:, 1],
                                                       self.Feat.vars[self.y_name[1]].max_thresh, self.Feat.vars[
                                                           self.y_name[1]].min_thresh)
        test_y_predict_time = self.denormalize_output(test_y_predict_n[:, 0], self.Feat.vars[self.y_name[0]].max_thresh,
                                                      self.Feat.vars[
                                                          self.y_name[0]].min_thresh)
        test_y_predict_temp = self.denormalize_output(test_y_predict_n[:, 1], self.Feat.vars[self.y_name[1]].max_thresh,
                                                      self.Feat.vars[
                                                          self.y_name[1]].min_thresh)

        real_y_0_time, real_y_1_time, real_y_0_temp, real_y_1_temp = self.denormalize_multiple_y()

        err_train_time = abs(train_y_predict_time - real_y_0_time)
        err_train_temp = abs(train_y_predict_temp - real_y_0_temp)
        err_test_time = abs(test_y_predict_time - real_y_1_time)
        err_test_temp = abs(test_y_predict_temp - real_y_1_temp)

        mean_squared_err_train_time = mean_squared_error(real_y_0_time, train_y_predict_time)
        mean_squared_err_train_temp = mean_squared_error(real_y_0_temp, train_y_predict_temp)
        mean_squared_err_test_time = mean_squared_error(real_y_1_time, test_y_predict_time)
        mean_squared_err_test_temp = mean_squared_error(real_y_1_temp, test_y_predict_temp)

        train_result_time = err_train_time <= float(dnn_cfg['time_est_margin'])
        train_result_temp = err_train_temp <= float(dnn_cfg['temp_est_margin'])
        test_result_time = err_test_time <= float(dnn_cfg['time_est_margin'])
        test_result_temp = err_test_temp <= float(dnn_cfg['temp_est_margin'])

        train_accuracy_time = 100. * sum(train_result_time) / float(len(train_result_time))
        train_accuracy_temp = 100. * sum(train_result_temp) / float(len(train_result_temp))
        test_accuracy_time = 100. * sum(test_result_time) / float(len(test_result_time))
        test_accuracy_temp = 100. * sum(test_result_temp) / float(len(test_result_temp))

        num_train = len(self.X[0])
        num_test = len(self.X[1])
        num_ratio = int(100. * num_train / float(num_train + num_test))

        msg += "{:6.2f}".format(train_accuracy_time)
        msg += "{:6.2f}".format(train_accuracy_temp)
        msg += "{:6.2f}".format(test_accuracy_time)
        msg += "{:6.2f}".format(test_accuracy_temp)
        msg += "{:6.2f}".format(mean_squared_err_train_time)
        msg += "{:6.2f}".format(mean_squared_err_train_temp)
        msg += "{:6.2f}".format(mean_squared_err_test_time)
        msg += "{:6.2f}".format(mean_squared_err_test_temp)

        print(msg)
        self.log = msg

        self.train_acc = train_accuracy_time
        self.train_acc_2 = train_accuracy_temp
        self.test_acc = test_accuracy_time
        self.test_acc_2 = test_accuracy_temp

        self.train_mse = mean_squared_err_train_time
        self.train_mse_2 = mean_squared_err_train_temp
        self.test_mse = mean_squared_err_test_time
        self.test_mse_2 = mean_squared_err_test_temp

        trans_dataset = posco.transpose_list(self.dataset[1:])
        self.X_dataset = posco.transpose_list(trans_dataset[1:])
        self.y_dataset = trans_dataset[0]

        self.params = dnn_cfg

        # # Plot outputs
        # plt.scatter(test_X_n[:, np.newaxis, 0], test_y_n, color='black')
        # plt.plot(test_X_n[:, np.newaxis, 0], test_y_predict_n, color='blue', linewidth=1)
        # plt.xticks(())
        # plt.yticks(())
        # plt.show()

    def light_gbm_regressor(self, lgbm_cfg):
        """ Run light gbm regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        msg = "\n # Light GBM Regression"
        n_estimators = int(lgbm_cfg['n_estimators'])
        learning_rate = float(lgbm_cfg['learning_rate'])
        est_thresh = float(lgbm_cfg['est_thresh'])

        self.acc_thresh = est_thresh

        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'learning_rate': [0.01, 0.1, 0.05, 0.5, 1],
                'n_estimators': [50, 100, 200, 300, 400, 500]
            }

            mdl = LGBMRegressor(n_estimators=n_estimators, learning_rate=learning_rate, n_jobs=-1)
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4f}' .format(grid.best_score_))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = LGBMRegressor(n_estimators=n_estimators, learning_rate=learning_rate)


        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []


        for i in range(2):
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
            mape_arr.append(posco.calc_MAPE(self.y[i], pred_y[-1]))
            rmse_arr.append(posco.calc_RMSE(self.y[i], pred_y[-1]))

        self.y_predict = pred_y
        self.train_acc = self.check_est_accuracy(est_err[0], est_thresh)
        self.test_acc = self.check_est_accuracy(est_err[1], est_thresh)
        self.train_mse = mean_squared_error(self.y[0], pred_y[0].tolist())
        self.test_mse = mean_squared_error(self.y[1], pred_y[1].tolist())

        msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        msg += "\n # train mse : {:4.2f}, test mse : {:4.2f}".format(self.train_mse, self.test_mse)

        self.log = msg
        self.params = lgbm_cfg

        if False:
            self.find_rfr_feature_importance(category_list=['OP_PATTERN'], num=1)

        # if True:
        #     self.gen_plot()

        return

    def xgboost_regressor(self, xgboost_cfg):
        """ Run xgboost regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        msg = "\n # XGBoost Regression"
        n_estimators = int(xgboost_cfg['n_estimators'])
        learning_rate = float(xgboost_cfg['learning_rate'])
        est_thresh = float(xgboost_cfg['est_thresh'])

        self.acc_thresh = est_thresh

        one_to_left = st.beta(10, 1)
        from_zero_positive = st.expon(0, 50)
        GRID_SEARCH_ = False
        if GRID_SEARCH_:
            param_grid = {
                'n_estimators': [500, 1000, 1500],# [50, 100, 200, 300, 400, 500],
                'learning_rate': [0.1], # [0.01, 0.1, 0.05, 0.5, 1],
                # 'max_depth' : [5, 10, 20, 40, 60],
                # 'colsample_bytree' : one_to_left,
                # 'subsample' : one_to_left,
                # 'gamma' : st.uniform(0, 10),
                # 'reg_alpha' : from_zero_positive,
                # 'min_child_weight' : from_zero_positive
            }

            mdl = XGBRegressor(n_estimators=n_estimators, learning_rate=learning_rate, n_jobs=-1)
            grid = GridSearchCV(estimator=mdl, param_grid=param_grid, cv=5)
            grid = grid.fit(self.X[0], self.y[0])
            print('Best score is {:4.4f}' .format(sum(grid.best_estimator_.predict(self.X[1]) == self.y[1]) / (len(self.y[1])*1.0)))
            print('Best params is {}' .format(str(grid.best_params_)))
            print('Parameter tuning complete')

        else:
            mdl = XGBRegressor(n_estimators=n_estimators, learning_rate=learning_rate)


        mdl.fit(self.X[0], self.y[0])
        self.model = mdl

        pred_y = []
        est_err = []
        est_acc = []
        mape_arr = []
        rmse_arr = []


        for i in range(2):
            pred_y.append(mdl.predict(self.X[i]))
            est_err.append(abs(pred_y[i] - np.array(self.y[i])))
            est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
            mape_arr.append(posco.calc_MAPE(self.y[i], pred_y[-1]))
            rmse_arr.append(posco.calc_RMSE(self.y[i], pred_y[-1]))

        self.y_predict = pred_y
        self.train_acc = self.check_est_accuracy(est_err[0], est_thresh)
        self.test_acc = self.check_est_accuracy(est_err[1], est_thresh)
        self.train_mse = mean_squared_error(self.y[0], pred_y[0].tolist())
        self.test_mse = mean_squared_error(self.y[1], pred_y[1].tolist())

        msg += "\n # train acc : {:4.2f}, test acc : {:4.2f}".format(self.train_acc, self.test_acc)
        msg += "\n # train mse : {:4.2f}, test mse : {:4.2f}".format(self.train_mse, self.test_mse)

        self.log = msg
        self.params = xgboost_cfg

        if False:
            self.find_rfr_feature_importance(category_list=['OP_PATTERN'], num=1)

        # if True:
        #     self.gen_plot()

        return

    def run(self, model, ml_cfg):
        if ml_cfg['TYPE']['name'] == 'RH_PLUS':
            self.split_dataset_into_train_and_test_for_multiple_y(dataset_ratio=int(ml_cfg['DNN']['train_ratio']),
                                                                  num_y=2)
            if model == "DNN":
                self.deep_neuralnet_regressor_for_multiple_y(ml_cfg['DNN'])
            else:
                self.random_forest_regressor_for_multiple_y(ml_cfg['RFR'])
        else:
            self.split_dataset_into_train_and_test(dataset_ratio=int(ml_cfg['DNN']['train_ratio']))
            if model == "DNN":
                self.deep_neuralnet_regressor(ml_cfg['DNN'])
            elif model == "RFR":
                self.random_forest_regressor(ml_cfg['RFR'])
            elif model == "LGBMR":
                self.light_gbm_regressor(ml_cfg['LGBMR'])
            elif model == "XGBR":
                self.xgboost_regressor(ml_cfg['XGBR'])

    def denormalize_y(self, min_val=-1, max_val=1):
        max_thresh = self.Feat.vars[self.y_name].max_thresh
        min_thresh = self.Feat.vars[self.y_name].min_thresh
        real_y_0 = []
        real_y_1 = []
        for val in self.y[0]:
            real_y_0.append( (val - min_val) / (max_val - min_val) * (max_thresh - min_thresh) + min_thresh)
        for val in self.y[1]:
            real_y_1.append( (val - min_val) / (max_val - min_val) * (max_thresh - min_thresh) + min_thresh)
        return real_y_0, real_y_1

    def denormalize_multiple_y(self, min_val=-1, max_val=1):
        max_thresh_time = self.Feat.vars[self.y_name[0]].max_thresh
        min_thresh_time = self.Feat.vars[self.y_name[0]].min_thresh
        max_thresh_temp = self.Feat.vars[self.y_name[1]].max_thresh
        min_thresh_temp = self.Feat.vars[self.y_name[1]].min_thresh
        real_y_0_time = []
        real_y_1_time = []
        real_y_0_temp = []
        real_y_1_temp = []

        trans_y_0 = posco.transpose_list(self.y[0])
        trans_y_1 = posco.transpose_list(self.y[1])

        for val in trans_y_0[0]:
            real_y_0_time.append(
                (val - min_val) / (max_val - min_val) * (max_thresh_time - min_thresh_time) + min_thresh_time)
        for val in trans_y_1[0]:
            real_y_1_time.append(
                (val - min_val) / (max_val - min_val) * (max_thresh_time - min_thresh_time) + min_thresh_time)
        for val in trans_y_0[1]:
            real_y_0_temp.append(
                (val - min_val) / (max_val - min_val) * (max_thresh_temp - min_thresh_temp) + min_thresh_temp)
        for val in trans_y_1[1]:
            real_y_1_temp.append(
                (val - min_val) / (max_val - min_val) * (max_thresh_temp - min_thresh_temp) + min_thresh_temp)
        return real_y_0_time, real_y_1_time, real_y_0_temp, real_y_1_temp

def MinMaxScaler(data):
    numerator = data - np.min(data)
    denominator = np.max(data) - np.min(data)
    return numerator / (denominator + 1e-7)


def NormToReal(given_output, predict_output):
    print(max(given_output), min(given_output))
    return predict_output * (max(given_output) - min(given_output)) + min(given_output)



