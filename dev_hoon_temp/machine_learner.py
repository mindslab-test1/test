#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
import sys
from operator import itemgetter
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor
from sklearn.svm import SVR
from sklearn.svm import NuSVR
from sklearn.svm import LinearSVR
import pickle
import numpy as np
import tensorflow as tf
import lib.posco as posco

__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

SVR_C = 1.0
SVR_epsilon = 0.1


class MachineLearner(object):
    """

    """
    def __init__(self, train_X, train_y, test_X, test_y, name_exist=False):
        self.name_exist = name_exist
        self.train_X = train_X[int(self.name_exist):]
        self.train_y = train_y[int(self.name_exist):]
        self.test_X = test_X[int(self.name_exist):]
        self.test_y = test_y[int(self.name_exist):]
        self.name_X = train_X[0:int(self.name_exist)][0]
        self.name_y = train_y[0:int(self.name_exist)][0]
        self.log = ""

    def get_log(self):
        return self.log

    def random_forest_regressor(self, est_margin=sys.float_info.epsilon,
                                plot_filename_prefix='rfr', plot_=False, pkl_filename='', feat_mdl=None):
        reg = RandomForestRegressor(n_estimators=20)
        reg.fit(self.train_X, self.train_y)
        train_y_predict = reg.predict(self.train_X)
        test_y_predict = reg.predict(self.test_X)
        err_train = train_y_predict - self.train_y
        err_test  = test_y_predict  - self.test_y
        err_train_abs = abs(err_train)
        err_test_abs  = abs(err_test)
        train_result = err_train_abs <= est_margin
        test_result  = err_test_abs  <= est_margin
        train_accuracy = 100. * sum(train_result) / float(train_result.shape[0])
        test_accuracy  = 100. * sum(test_result)  / float(test_result.shape[0])

        feat_impt = [[self.name_X[idx], reg.feature_importances_[idx]] for idx in range(len(self.name_X))]
        sorted_feat_impt = sorted(feat_impt, key=itemgetter(1), reverse=True)

        num_train = len(self.train_X)
        num_test  = len(self.test_X)
        num_ratio = int(100. * num_train / float(num_train + num_test))
        msg  = "\n"
        msg += "\n # Machine learning by random forest regressor"
        msg += "\n   > Train vs test = {:d} vs {:d} ({:d}%)".format(num_train, num_test, num_ratio)
        msg += "\n   > Train dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_train_abs.mean(),
                                                                                       err_train_abs.var())
        msg += "\n   > Test  dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_train_abs.mean(),
                                                                                       err_train_abs.var())
        msg += "\n   > Estimator accuracy for train dataset = {:6.2f}".format(train_accuracy)
        msg += "\n   > Estimator accuracy for test  dataset = {:6.2f}".format(test_accuracy)
        msg += "\n   > Sorted feature importance"
        for content in sorted_feat_impt:
            msg += "\n   > {:<30} : {:6d}".format(content[0], int(content[1] * 100000))
        self.log = msg

        max_range = max(max(err_train_abs), max(err_test_abs), est_margin) + 1
        fig = plt.figure()
        plt.subplot(2,1,1)
        plt.plot(err_train_abs, 'b', [est_margin,] * len(err_train_abs), 'r')
        plt.ylim(0, max_range)
        plt.title('Training ABS error({:2d}%)'.format(int(train_accuracy)))
        plt.subplot(2,1,2)
        plt.plot(err_test_abs, 'b', [est_margin,] * len(err_test_abs), 'r')
        plt.ylim(0, max_range)
        plt.title('Test ABS error({:2d}%)'.format(int(test_accuracy)))
        fig.tight_layout()
        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix + 'time.png')
        if plot_:
            plt.show()
        plt.close(fig)

        fig = plt.figure()
        plt.subplot(2,1,1)
        y, _, _ = plt.hist(err_train, 256, facecolor='blue')
        axes = plt.gca()
        axes.set_xlim([-est_margin*2,est_margin*2])
        axes.set_ylim([0, y.max()])
        plt.title('Histogram of training error')
        plt.subplot(2,1,2)
        plt.hist(err_test,  256, facecolor='blue')
        axes = plt.gca()
        axes.set_xlim([-est_margin*2,est_margin*2])
        axes.set_ylim([0, y.max()])
        plt.title('Histogram of test error')
        fig.tight_layout()
        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix + 'hist.png')
        if plot_:
            plt.show()
        plt.close(fig)

        est_thresholds = np.arange(0, est_margin*2, 0.1)
        train_acc = []
        test_acc = []
        for thresh in est_thresholds:
            train_acc.append(100. * sum(err_train_abs <= thresh) / err_train_abs.shape[0])
            test_acc.append(100. * sum(err_test_abs  <= thresh) / err_test_abs.shape[0])
        fig = plt.figure()
        plt.plot(est_thresholds, train_acc, 'b-', label='train')
        plt.plot(est_thresholds, test_acc,  'r-', label='test')
        plt.title('Performance variation according to tolerance')
        plt.grid(True)
        plt.legend(loc='lower right', shadow=True)
        fig.tight_layout()
        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix + 'perf.png')
        if plot_:
            plt.show()
        plt.close(fig)

        if pkl_filename:
            pickle.dump([reg, self.name_X, self.name_y, msg, feat_mdl], open(pkl_filename, 'wb'))

        pass

    def support_vector_regressor(self, est_margin=sys.float_info.epsilon, plot_filename_prefix='', plot_=True, pkl_filename=''):
        # reg = SVR(C=SVR_C, epsilon=SVR_epsilon)
        reg = LinearSVR()
        reg.fit(self.train_X, self.train_y)
        train_y_predict = reg.predict(self.train_X)
        test_y_predict = reg.predict(self.test_X)
        err_train = abs(train_y_predict - self.train_y)
        err_test  = abs(test_y_predict  - self.test_y)
        train_result = err_train <= est_margin
        test_result  = err_test  <= est_margin
        train_accuracy = 100. * sum(train_result) / float(len(train_result))
        test_accuracy  = 100. * sum(test_result)  / float(len(test_result))

        num_train = len(self.train_X)
        num_test  = len(self.test_X)
        num_ratio = int(100. * num_train / float(num_train + num_test))
        msg  = "\n"
        msg += "\n # Machine learning by support vector regressor"
        msg += "\n   > Train vs test = {:d} vs {:d} ({:d}%)".format(num_train, num_test, num_ratio)
        msg += "\n   > Train dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_train.mean(), err_train.var())
        msg += "\n   > Test  dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_test.mean(),  err_test.var())
        msg += "\n   > Estimator accuracy for train dataset = {:6.2f}".format(train_accuracy)
        msg += "\n   > Estimator accuracy for test  dataset = {:6.2f}".format(test_accuracy)
        self.log = msg
        print(msg)
        print(" real  = ({:f} {:f}) -> ({:f} {:f})".format(min(self.train_y), max(self.train_y),
                                                           min(train_y_predict), max(train_y_predict)))
        print(" real  = ({:f} {:f}) -> ({:f} {:f})".format(min(self.test_y), max(self.test_y),
                                                           min(test_y_predict), max(test_y_predict)))

        max_range = max(max(err_train), max(err_test), est_margin) + 1
        fig = plt.figure()
        plt.subplot(1,2,1)
        plt.plot(err_train, 'b', [est_margin,] * len(err_train), 'r')
        plt.ylim(0, max_range)
        plt.title('Training ABS error({:2d}%)'.format(int(train_accuracy)))
        plt.subplot(1,2,2)
        plt.plot(err_test, 'b', [est_margin,] * len(err_test), 'r')
        plt.ylim(0, max_range)
        plt.title('Test ABS error({:2d}%)'.format(int(test_accuracy)))
        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix)
        if plot_:
            plt.show()
        else:
            plt.close(fig)

        if pkl_filename:
            pickle.dump([reg, self.name_X, self.name_y, msg], open(pkl_filename, 'wb'))
        pass

    def deep_neuralnet_regressor(self, est_margin=sys.float_info.epsilon, plot_filename_prefix='', plot_=True, pkl_filename=''):

        trans_train_X = posco.transpose_list(self.train_X)
        trans_test_X = posco.transpose_list(self.test_X)

        train_X_n = []
        test_X_n = []
        dat_min = []
        dat_max = []

        for idx in range(len(trans_train_X)):
            train_X_n.append(MinMaxScaler(trans_train_X[idx]))
            test_X_n.append(MinMaxScaler(trans_test_X[idx]))
            dat_min.append(min(trans_train_X[idx]))
            dat_max.append(max(trans_train_X[idx]))

        output_min = min(self.train_y)
        output_max = max(self.train_y)

        train_X_n = posco.transpose_list(train_X_n)
        test_X_n = posco.transpose_list(test_X_n)
        train_y_n = MinMaxScaler(self.train_y)
        test_y_n = MinMaxScaler(self.test_y)

        train_X_n = np.array(train_X_n)
        test_X_n = np.array(test_X_n)
        train_y_n = np.array(train_y_n)
        test_y_n = np.array(test_y_n)
        train_y_n.shape = (train_y_n.shape[0], 1)
        test_y_n.shape = (test_y_n.shape[0], 1)

        def get_train_data():
            X_train_tf = tf.constant(train_X_n)
            Y_train_tf = tf.constant(train_y_n)
            return X_train_tf, Y_train_tf

        nsteps = 15001

        dnn_num_feat = train_X_n.shape[1]
        dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=dnn_num_feat)]
        dnn_active_fn = tf.tanh
        dnn_unit = [2]
        dnn_dir = "dnn_model"
        dnn_learn_rate = 0.2
        dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)


        msg = "\n"
        msg += "------------------------------------\n"

        reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col, activation_fn=dnn_active_fn,
                                            hidden_units=dnn_unit, model_dir=dnn_dir,
                                            optimizer=dnn_optimizer)


        reg.fit(input_fn=get_train_data, steps=nsteps)
        train_y_predict_n = reg.predict(train_X_n)
        train_y_predict_n = np.array(list(train_y_predict_n))
        test_y_predict_n = reg.predict(test_X_n)
        test_y_predict_n = np.array(list(test_y_predict_n))
        train_y_predict = NormToReal(self.train_y, train_y_predict_n)
        test_y_predict = NormToReal(self.train_y, test_y_predict_n)
        err_train = abs(train_y_predict - self.train_y)
        err_test = abs(test_y_predict - self.test_y)
        train_result = err_train <= est_margin
        test_result = err_test <= est_margin
        train_accuracy = 100. * sum(train_result) / float(len(train_result))
        test_accuracy = 100. * sum(test_result) / float(len(test_result))

        num_train = len(self.train_X)
        num_test = len(self.test_X)
        num_ratio = int(100. * num_train / float(num_train + num_test))

        msg += "\n   > train accuracy = {:6.2f}".format(train_accuracy)
        msg += "\n   > test accuracy = {:6.2f}".format(test_accuracy)

        print(msg)
        self.log = msg

        max_range = max(max(err_train), max(err_test), est_margin) + 1
        fig = plt.figure()
        plt.subplot(1, 2, 1)
        plt.plot(err_train, 'b', [est_margin, ] * len(err_train), 'r')
        plt.ylim(0, max_range)
        plt.title('Training ABS error({:2d}%)'.format(int(train_accuracy)))
        plt.subplot(1, 2, 2)
        plt.plot(err_test, 'b', [est_margin, ] * len(err_test), 'r')
        plt.ylim(0, max_range)
        plt.title('Test ABS error({:2d}%)'.format(int(test_accuracy)))

        dat_minmax = []
        dat_minmax.append(self.name_X)
        dat_minmax.append(dat_min)
        dat_minmax.append(dat_max)

        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix)
        if plot_:
            plt.show()
        else:
            plt.close(fig)

        if pkl_filename:
            pickle.dump([self.name_X, self.name_y, msg, dat_minmax, output_min, output_max, dnn_learn_rate],
                        open(pkl_filename, 'wb'))

        pass


def MinMaxScaler(data):
    numerator = data - np.min(data)
    denominator = np.max(data) - np.min(data)
    return numerator / (denominator + 1e-7)


def NormToReal(given_output, predict_output):
    print(max(given_output), min(given_output))
    return predict_output * (max(given_output) - min(given_output)) + min(given_output)
