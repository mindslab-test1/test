#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""This module's docstring summary line.
This is a multi-line docstring. Paragraphs are separated with blank lines.
Lines conform to 79-column limit.
Module and packages names should be short, lower_case_with_underscores.
Notice that this in not PEP8-cheatsheet.py
Seriously, use flake8. Atom.io with https://atom.io/packages/linter-flake8
is awesome!
See http://www.python.org/dev/peps/pep-0008/ for more PEP-8 details

Coding style reference
    - https://www.python.org/dev/peps/pep-0008/
    - http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu
    /PythonGuidelines.html

    WHAT TO DO
    > Normalization before learning.
    > RFR, SVR, DNR
    > apply string features to estimation.

"""
import os
import sys
import shutil
import random
import argparse
import configparser
import copy
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from operator import itemgetter
from lib.posco import read_csv_file
import pickle

from dev_hoon_temp import dataset_handler as Dataset
from dev_hoon_temp import machine_learner_2 as Learner

from dev_hoon_temp.dnn_learner import DnnLearner
import lib.posco as posco


__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

LEARNING_TEST = True
RESULT_FILE = "result.txt"


def main():
    """ Learning handler main code.

    """
    global RESULT_FILE

    parser = argparse.ArgumentParser()
    parser.add_argument("--var_csv_file", required=True, help="csv file for variables")
    parser.add_argument("--var_ini_file", required=True, help="ini file for configuration")
    parser.add_argument("--dataset_csv_file", default="", help="csv file for data set")
    parser.add_argument("--result_file", default="", help="Result filename")

    parser.add_argument("--machine_learning_", default=False, action='store_true',
                        help="flag to run machine learning analysis")

    args = parser.parse_args()

    if args.result_file:
        RESULT_FILE = args.result_file
    with open(RESULT_FILE, 'w') as fid:
        fid.write("\n # Data handling results")

    cfg = configparser.ConfigParser()
    cfg.read(args.var_ini_file)

    var_mtx = read_csv_file(args.var_csv_file)
    start_pos, end_pos = Dataset.calc_crop_info_from_ini(cfg[Dataset.VAR_INFO_CSV])
    roi_var_mtx = Dataset.crop_mtx(var_mtx, start_pos, end_pos)
    Feat = Dataset.PoscoTempEstModel(roi_var_mtx)
    Feat.init_feat_class(cfg, offset=start_pos[0])

    dataset = read_csv_file(args.dataset_csv_file)

    Learn = Learner.MachineLearner(Feat=Feat, dataset=dataset, out_idx=0)
    Learn.encode_symbol_dataset_by_int()
    Learn.run_random_forest_regressor(dataset_ratio=70,
                                      n_estimators=[20])  # list(np.arange(5,101,5)))
    posco.print_and_write(Learn.log, console=True, filename=RESULT_FILE)
    Learn.dataset = None
    pickle.dump(Learn, open("RFR.bin", 'wb'))

    pass


if __name__ == "__main__":

    if len(sys.argv) == 1:
        if LEARNING_TEST:
            sys.argv.extend(["--var_ini_file", "rh_var.ini",
                             "--var_csv_file", "rh_var.csv",
                             "--dataset_csv_file", "dataset/rh_dataset_all.csv",
                             "--result_file", "rh_result.txt",
                             "--machine_learning_",

                             # "--gen_db_table_",
                             # "--rename_dataset_col", "renamed_dataset.csv",
                             # "--hist_folder", HIST_FOLDER,
                             # "--test_stream_file", STREAM_FILE,
                             # "--gen_dataset_",
                             ])
        else:
            sys.argv.extend(["--~help"])
    main()
