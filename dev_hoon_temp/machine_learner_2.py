#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
import sys
import matplotlib
from operator import itemgetter
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor
# from sklearn.svm import SVR
# from sklearn.svm import NuSVR
from sklearn.svm import LinearSVR
import pickle
import numpy as np
import tensorflow as tf
import lib.posco as posco
import dev_hoon_temp.dataset_handler as Dataset

__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

SVR_C = 1.0
SVR_epsilon = 0.1


class MachineLearner(object):
    """

    """
    def __init__(self, Feat=None, dataset=None, out_idx=0):
        self.Feat = Feat
        self.dataset = dataset
        self.out_idx = 0
        self.log = ""
        self.est_thresh = 0.0

        self.X_name = None
        self.y_name = None
        self.X_dataset = None
        self.y_dataset = None
        self.y_predict = None

        self.mse = 0
        self.acc = 0
        self.acc_thresh = 0

        self.X = [[],[]]
        self.y = [[],[]]

        self.RFR_num_estimators = 0
        self.RFR_model = None

    def get_log(self):
        return self.log

    def split_dataset_into_train_and_test(self, dataset_ratio=70):
        """ Split dataset into train and test.

        :param dataset_ratio:
        :return:
        """
        dataset_train, dataset_test = Dataset.split_dataset_into_train_and_test(self.dataset, dataset_ratio, offset=1)
        trans_dataset_train = posco.transpose_list(dataset_train)
        trans_dataset_test  = posco.transpose_list(dataset_test)
        self.X[0] = posco.transpose_list(trans_dataset_train[1:])[1:]
        self.X[1] = posco.transpose_list(trans_dataset_test[1:])[1:]
        self.y[0] = trans_dataset_train[0][1:]
        self.y[1] = trans_dataset_test[0][1:]
        self.X_name = self.dataset[0][1:]
        self.y_name = self.dataset[0][0]

    def encode_symbol_dataset_by_int(self, dataset=None):
        """ Encode symbol dataset by integer

        :param dataset:
        :return:
        """
        if not dataset:
            dataset = self.dataset

        trans_dataset = posco.transpose_list(dataset)
        arr = []
        for col in trans_dataset:
            if self.Feat.is_number_class(col[0]):
                arr.append([col[0]] + [float(x) for x in col[1:]])
            else:
                arr.append([col[0]] + [self.Feat.vars[col[0]].domain.index(x) for x in col[1:]])

        self.dataset = posco.transpose_list(arr)

        return self.dataset

    def check_est_accuracy(self, error_arr, est_thresh=None):
        """ Check estimation accuracy based on estimation threshold.

        :param error_arr:
        :param est_thresh:
        :return:
        """
        if not est_thresh:
            est_thresh = self.est_thresh

        acc_arr = [x < est_thresh for x in error_arr]

        if len(acc_arr) == 0:
            return 99.99
        else:
            return 100. * sum(acc_arr) / float(len(acc_arr))

    def run_random_forest_regressor(self,
                                    dataset_ratio=70,
                                    n_estimators=None,
                                    est_thresh=7
                                    ):
        """ Run random forest regressor with various environments.

        :param dataset_ratio:
        :param n_estimators:
        :param est_thresh:
        :return:
        """
        msg = "\n # Random Forest Regression"

        if not n_estimators:
            n_estimators = [10]

        self.split_dataset_into_train_and_test(dataset_ratio=dataset_ratio)

        rst_arr = []
        for n_estimator in n_estimators:
            rst_arr.append([n_estimator])
            mdl = RandomForestRegressor(n_estimators=n_estimator)
            mdl.fit(self.X[0], self.y[0])
            self.RFR_num_estimators = n_estimator
            self.RFR_model = mdl
            pred_y  = []
            est_err = []
            est_acc = []
            mape_arr = []
            rmse_arr = []
            sigma = [0, 0]
            # sigma = [3,2]   # 96.52 / 92.09
            # sigma = [3.7, 2.1]  # 93.64 / 88.96
            # sigma = [4,3]   # 90.37 / 87.37
            # sigma = [5,4]   # 81.80 / 81.38
            # sigma = [6,5]   # 74.38 / 74.91
            # sigma = [3.5, 2.5]  # 93.97 / 90.23
            sigma = [5, 4.5]

            for i in range(2):
                pred_y.append(mdl.predict(self.X[i]))
                # pred_y.append(mdl.predict(self.X[i]) + np.random.normal(0,sigma[i],len(self.X[i])))
                est_err.append(abs(pred_y[i] - np.array(self.y[i])))
                est_acc.append(self.check_est_accuracy(est_err[-1], est_thresh))
                mape_arr.append(posco.calc_MAPE(self.y[i], pred_y[-1]))
                rmse_arr.append(posco.calc_RMSE(self.y[i], pred_y[-1]))
                rst_arr[-1].append(est_err[-1])
            feat_impt = [[self.dataset[0][idx][1:], mdl.feature_importances_[idx]] for idx in range(len(self.dataset[0][1:]))]
            feat_impt = sorted(feat_impt, key=itemgetter(1), reverse=True)
            rst_arr[-1].append(feat_impt)
            rst_arr[-1].insert(1, est_acc[0])
            rst_arr[-1].insert(2, est_acc[1])

            print(" # RFR processing with n_estimation = {:d}".format(n_estimator))
            msg += "\n\n # n_estimator : {:d}".format(n_estimator)
            msg += "\n   |          |  train # |   test #  |  train % |  test %  |"
            msg += "\n   | Overall  |  {:6d}  |   {:6d}  |".format(est_err[0].shape[0], est_err[1].shape[0])
            msg +=                  "  {:6.2f}  |  {:6.2f}  |".format(self.check_est_accuracy(est_err[0], est_thresh),
                                                                      self.check_est_accuracy(est_err[1], est_thresh))
            if False:   # This is a routine to draw the plot to show the accuracy performance according to estimators.
                n_estimators = [x[0] for x in rst_arr]
                train_acc = [x[1] for x in rst_arr]
                test_acc = [x[2] for x in rst_arr]
                plt.plot(n_estimators, train_acc, 'b', label='Train')
                plt.plot(n_estimators, test_acc, 'r', label='Test')
                plt.ylim(50,110)
                plt.xticks(n_estimators)
                plt.yticks(list(np.arange(50, 105, 5)))
                plt.legend()
                plt.xlabel("Number of tree in the forest")
                plt.ylabel("Estimation accuracy with 7 degree")
                plt.title("Performance variation according to the number of tree")
                plt.savefig("RFR_performance_variation.png")

            if False:
                bar_name = []
                cnt = 0
                for content in feat_impt:
                    plt.barh(cnt, content[1])
                    bar_name.append(content[0])
                    cnt += 1
                    if cnt > 10:
                        break
                plt.xlim(0, feat_impt[0][1]*1.2)
                plt.xlabel('Feature importance')
                plt.ylabel('Features')
                plt.yticks([i for i, _ in enumerate(bar_name)], bar_name)
                plt.title('Random Forest Classifier')
                matplotlib.rcParams.update({'font.size': 22})
                plt.savefig('RFR_feat_impt.png')

            if False:
                trans_train_X = posco.transpose_list(self.X[0])
                trans_test_X = posco.transpose_list(self.X[1])
                for k in range(len(self.X_name)):
                    key = self.X_name[k]
                    if not self.Feat.is_number_class(key):
                        domain_len = len(self.Feat.vars[key].domain)
                        msg += "\n # analyzing estimation performance based on {} ({:d})".format(key, domain_len)
                        print("\n # analyzing estimation performance based on {} ({:d})".format(key, domain_len))
                        train_cnt = []
                        train_err = []
                        train_acc = []
                        test_cnt = []
                        test_err = []
                        test_acc = []
                        for i in range(domain_len):
                            train_cnt.append(0)
                            train_err.append([])
                            train_acc.append(0)
                            for j in range(len(self.X[0])):
                                if int(trans_train_X[k][j]) == int(i):
                                    train_cnt[-1] += 1
                                    train_err[-1].append(est_err[0][j])
                            train_acc[-1] = self.check_est_accuracy(train_err[-1], est_thresh)
                            test_cnt.append(0)
                            test_err.append([])
                            test_acc.append(0)
                            for j in range(len(self.X[1])):
                                if int(trans_test_X[k][j]) == int(i):
                                    test_cnt[-1] += 1
                                    test_err[-1].append(est_err[1][j])
                            test_acc[-1] = self.check_est_accuracy(test_err[-1], est_thresh)
                            msg += "\n   | {:>8} ".format(self.Feat.vars[key].domain[i])
                            msg +=      "|  {:6d}  |  {:6d}  ".format(train_cnt[-1], test_cnt[-1])
                            msg +=      "|  {:6.2f}  |   {:6.2f}  |".format(train_acc[-1], test_acc[-1])
                        msg += "\n   |          |  {:6d}  |  {:6d}  |".format(sum(train_cnt), sum(test_cnt))

                        if False:
                            x_range = list(np.arange(domain_len))
                            plt.plot(x_range, train_acc, 'b', label='Train')
                            plt.plot(x_range, test_acc, 'r', label='Test')
                            plt.ylim(80,110)
                            plt.xticks(x_range)
                            plt.yticks(list(np.arange(80, 101, 1)))
                            plt.legend()
                            plt.xlabel("Symbol index")
                            plt.ylabel("Estimation accuracy with 7 degree")
                            plt.title("Performance variation according to symbol in {}".format(key))
                            plt.tight_layout()
                            plt.savefig("perf_variation_{}.png".format(key))

        self.log = msg
        return


"""
        num_train = len(self.train_X)
        num_test  = len(self.test_X)
        num_ratio = int(100. * num_train / float(num_train + num_test))
        msg  = "\n"
        msg += "\n # Machine learning by random forest regressor"
        msg += "\n   > Train vs test = {:d} vs {:d} ({:d}%)".format(num_train, num_test, num_ratio)
        msg += "\n   > Train dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_train_abs.mean(),
                                                                                       err_train_abs.var())
        msg += "\n   > Test  dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_train_abs.mean(),
                                                                                       err_train_abs.var())
        msg += "\n   > Estimator accuracy for train dataset = {:6.2f}".format(train_accuracy)
        msg += "\n   > Estimator accuracy for test  dataset = {:6.2f}".format(test_accuracy)
        msg += "\n   > Sorted feature importance"
        for content in sorted_feat_impt:
            msg += "\n   > {:<30} : {:6d}".format(content[0], int(content[1] * 100000))
        self.log = msg

        max_range = max(max(err_train_abs), max(err_test_abs), est_margin) + 1
        fig = plt.figure()
        plt.subplot(2,1,1)
        plt.plot(err_train_abs, 'b', [est_margin,] * len(err_train_abs), 'r')
        plt.ylim(0, max_range)
        plt.title('Training ABS error({:2d}%)'.format(int(train_accuracy)))
        plt.subplot(2,1,2)
        plt.plot(err_test_abs, 'b', [est_margin,] * len(err_test_abs), 'r')
        plt.ylim(0, max_range)
        plt.title('Test ABS error({:2d}%)'.format(int(test_accuracy)))
        fig.tight_layout()
        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix + 'time.png')
        if plot_:
            plt.show()
        plt.close(fig)

        fig = plt.figure()
        plt.subplot(2,1,1)
        y, _, _ = plt.hist(err_train, 256, facecolor='blue')
        axes = plt.gca()
        axes.set_xlim([-est_margin*2,est_margin*2])
        axes.set_ylim([0, y.max()])
        plt.title('Histogram of training error')
        plt.subplot(2,1,2)
        plt.hist(err_test,  256, facecolor='blue')
        axes = plt.gca()
        axes.set_xlim([-est_margin*2,est_margin*2])
        axes.set_ylim([0, y.max()])
        plt.title('Histogram of test error')
        fig.tight_layout()
        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix + 'hist.png')
        if plot_:
            plt.show()
        plt.close(fig)

        est_thresholds = np.arange(0, est_margin*2, 0.1)
        train_acc = []
        test_acc = []
        for thresh in est_thresholds:
            train_acc.append(100. * sum(err_train_abs <= thresh) / err_train_abs.shape[0])
            test_acc.append(100. * sum(err_test_abs  <= thresh) / err_test_abs.shape[0])
        fig = plt.figure()
        plt.plot(est_thresholds, train_acc, 'b-', label='train')
        plt.plot(est_thresholds, test_acc,  'r-', label='test')
        plt.title('Performance variation according to tolerance')
        plt.grid(True)
        plt.legend(loc='lower right', shadow=True)
        fig.tight_layout()
        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix + 'perf.png')
        if plot_:
            plt.show()
        plt.close(fig)

        if pkl_filename:
            pickle.dump([mdl, self.name_X, self.name_y, msg, feat_mdl], open(pkl_filename, 'wb'))

        pass

    def support_vector_regressor(self, est_margin=sys.float_info.epsilon, plot_filename_prefix='', plot_=True, pkl_filename=''):
        # reg = SVR(C=SVR_C, epsilon=SVR_epsilon)
        reg = LinearSVR()
        reg.fit(self.train_X, self.train_y)
        train_y_predict = reg.predict(self.train_X)
        test_y_predict = reg.predict(self.test_X)
        err_train = abs(train_y_predict - self.train_y)
        err_test  = abs(test_y_predict  - self.test_y)
        train_result = err_train <= est_margin
        test_result  = err_test  <= est_margin
        train_accuracy = 100. * sum(train_result) / float(len(train_result))
        test_accuracy  = 100. * sum(test_result)  / float(len(test_result))

        num_train = len(self.train_X)
        num_test  = len(self.test_X)
        num_ratio = int(100. * num_train / float(num_train + num_test))
        msg  = "\n"
        msg += "\n # Machine learning by support vector regressor"
        msg += "\n   > Train vs test = {:d} vs {:d} ({:d}%)".format(num_train, num_test, num_ratio)
        msg += "\n   > Train dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_train.mean(), err_train.var())
        msg += "\n   > Test  dataset statistics: mean = {:5.2f}, var = {:7.2f}".format(err_test.mean(),  err_test.var())
        msg += "\n   > Estimator accuracy for train dataset = {:6.2f}".format(train_accuracy)
        msg += "\n   > Estimator accuracy for test  dataset = {:6.2f}".format(test_accuracy)
        self.log = msg
        print(msg)
        print(" real  = ({:f} {:f}) -> ({:f} {:f})".format(min(self.train_y), max(self.train_y),
                                                           min(train_y_predict), max(train_y_predict)))
        print(" real  = ({:f} {:f}) -> ({:f} {:f})".format(min(self.test_y), max(self.test_y),
                                                           min(test_y_predict), max(test_y_predict)))

        max_range = max(max(err_train), max(err_test), est_margin) + 1
        fig = plt.figure()
        plt.subplot(1,2,1)
        plt.plot(err_train, 'b', [est_margin,] * len(err_train), 'r')
        plt.ylim(0, max_range)
        plt.title('Training ABS error({:2d}%)'.format(int(train_accuracy)))
        plt.subplot(1,2,2)
        plt.plot(err_test, 'b', [est_margin,] * len(err_test), 'r')
        plt.ylim(0, max_range)
        plt.title('Test ABS error({:2d}%)'.format(int(test_accuracy)))
        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix)
        if plot_:
            plt.show()
        else:
            plt.close(fig)

        if pkl_filename:
            pickle.dump([reg, self.name_X, self.name_y, msg], open(pkl_filename, 'wb'))
        pass

    def deep_neuralnet_regressor(self, est_margin=sys.float_info.epsilon, plot_filename_prefix='', plot_=True, pkl_filename=''):

        trans_train_X = posco.transpose_list(self.train_X)
        trans_test_X = posco.transpose_list(self.test_X)

        train_X_n = []
        test_X_n = []
        dat_min = []
        dat_max = []

        for idx in range(len(trans_train_X)):
            train_X_n.append(MinMaxScaler(trans_train_X[idx]))
            test_X_n.append(MinMaxScaler(trans_test_X[idx]))
            dat_min.append(min(trans_train_X[idx]))
            dat_max.append(max(trans_train_X[idx]))

        output_min = min(self.train_y)
        output_max = max(self.train_y)

        train_X_n = posco.transpose_list(train_X_n)
        test_X_n = posco.transpose_list(test_X_n)
        train_y_n = MinMaxScaler(self.train_y)
        test_y_n = MinMaxScaler(self.test_y)

        train_X_n = np.array(train_X_n)
        test_X_n = np.array(test_X_n)
        train_y_n = np.array(train_y_n)
        test_y_n = np.array(test_y_n)
        train_y_n.shape = (train_y_n.shape[0], 1)
        test_y_n.shape = (test_y_n.shape[0], 1)

        def get_train_data():
            X_train_tf = tf.constant(train_X_n)
            Y_train_tf = tf.constant(train_y_n)
            return X_train_tf, Y_train_tf

        nsteps = 15001

        dnn_num_feat = train_X_n.shape[1]
        dnn_feat_col = [tf.contrib.layers.real_valued_column("", dimension=dnn_num_feat)]
        dnn_active_fn = tf.tanh
        dnn_unit = [2]
        dnn_dir = "dnn_model"
        dnn_learn_rate = 0.2
        dnn_optimizer = tf.train.AdagradOptimizer(learning_rate=dnn_learn_rate)


        msg = "\n"
        msg += "------------------------------------\n"

        reg = tf.contrib.learn.DNNRegressor(feature_columns=dnn_feat_col, activation_fn=dnn_active_fn,
                                            hidden_units=dnn_unit, model_dir=dnn_dir,
                                            optimizer=dnn_optimizer)


        reg.fit(input_fn=get_train_data, steps=nsteps)
        train_y_predict_n = reg.predict(train_X_n)
        train_y_predict_n = np.array(list(train_y_predict_n))
        test_y_predict_n = reg.predict(test_X_n)
        test_y_predict_n = np.array(list(test_y_predict_n))
        train_y_predict = NormToReal(self.train_y, train_y_predict_n)
        test_y_predict = NormToReal(self.train_y, test_y_predict_n)
        err_train = abs(train_y_predict - self.train_y)
        err_test = abs(test_y_predict - self.test_y)
        train_result = err_train <= est_margin
        test_result = err_test <= est_margin
        train_accuracy = 100. * sum(train_result) / float(len(train_result))
        test_accuracy = 100. * sum(test_result) / float(len(test_result))

        num_train = len(self.train_X)
        num_test = len(self.test_X)
        num_ratio = int(100. * num_train / float(num_train + num_test))

        msg += "\n   > train accuracy = {:6.2f}".format(train_accuracy)
        msg += "\n   > test accuracy = {:6.2f}".format(test_accuracy)

        print(msg)
        self.log = msg

        max_range = max(max(err_train), max(err_test), est_margin) + 1
        fig = plt.figure()
        plt.subplot(1, 2, 1)
        plt.plot(err_train, 'b', [est_margin, ] * len(err_train), 'r')
        plt.ylim(0, max_range)
        plt.title('Training ABS error({:2d}%)'.format(int(train_accuracy)))
        plt.subplot(1, 2, 2)
        plt.plot(err_test, 'b', [est_margin, ] * len(err_test), 'r')
        plt.ylim(0, max_range)
        plt.title('Test ABS error({:2d}%)'.format(int(test_accuracy)))

        dat_minmax = []
        dat_minmax.append(self.name_X)
        dat_minmax.append(dat_min)
        dat_minmax.append(dat_max)

        if plot_filename_prefix:
            fig.savefig(plot_filename_prefix)
        if plot_:
            plt.show()
        else:
            plt.close(fig)

        if pkl_filename:
            pickle.dump([self.name_X, self.name_y, msg, dat_minmax, output_min, output_max, dnn_learn_rate],
                        open(pkl_filename, 'wb'))

        pass


def MinMaxScaler(data):
    numerator = data - np.min(data)
    denominator = np.max(data) - np.min(data)
    return numerator / (denominator + 1e-7)


def NormToReal(given_output, predict_output):
    print(max(given_output), min(given_output))
    return predict_output * (max(given_output) - min(given_output)) + min(given_output)

"""
