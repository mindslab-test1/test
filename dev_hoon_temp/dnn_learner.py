#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
import sys
from operator import itemgetter
import matplotlib.pyplot as plt
import pickle
import numpy as np
import tensorflow as tf
import lib.posco as lib


__author__ = "Hoon Paek, Dong-gi Kim, Ji-young Moon"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = []
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Hoon Paek"
__email__ = "hoon.paek@mindslab.ai"
__status__ = "Prototype"  # Prototype / Development / Production

SVR_C = 1.0
SVR_epsilon = 0.1


class DnnLearner(object):
    """

    """
    def __init__(self, train_X, train_y, test_X, test_y, name_exist=False):
        self.name_exist = name_exist
        self.train_X = np.array(train_X[int(self.name_exist):], dtype=np.float32)
        self.train_y = np.array(train_y[int(self.name_exist):], dtype=np.float32)
        self.test_X = np.array(test_X[int(self.name_exist):], dtype=np.float32)
        self.test_y = np.array(test_y[int(self.name_exist):], dtype=np.float32)
        self.name_X = train_X[0:int(self.name_exist)][0]
        self.name_y = train_y[0:int(self.name_exist)][0]
        self.log = ""

    def get_tf_model(self, hidden_units):
        """ Setup our Deep Neural Network and LOAD any existing model

            Returns:
              ( Our (maybe trained) model, a helper input function)
        """
        # FEATURES is a short description for each column
        # of your data.  Change this to match your data
        # FEATURES = ["x1", "x2"]
        FEATURES = self.name_X
        # Set this to describe your columns.  If they are all real values,
        # you don't need to change it.
        feature_columns = [tf.contrib.layers.real_valued_column(k) for k in FEATURES]

        # Build 3 layer DNN.  You can change this however you want, or use a
        # linear regressor, or use a classifier etc.
        # NOTE:
        #   This will LOAD any existing model in the "model_dir" directory!
        # The documentation fails to mention this point as of writing
        regressor = tf.contrib.learn.DNNRegressor(feature_columns=feature_columns,
                                                  hidden_units=hidden_units,    # [128, 128, 128],
                                                  model_dir="tf_model")

        def input_fn(x_data, y_data=None):
            # Note the 'shape' parameter is to suppress a very noisy warning.
            # You can probably remove this parameter in a month or two.
            feature_cols = {k: tf.constant(x_data[:, i], shape=[len(x_data[:, i]), 1])
                            for i, k in enumerate(FEATURES)}
            if y_data is None:
                return feature_cols
            labels = tf.constant(y_data)
            return feature_cols, labels

        return regressor, input_fn

    def tf_train(self, regressor, input_fn, steps=1000):

        print("Training model ...")
        # Fit model.
        regressor.fit(input_fn=lambda: input_fn(self.train_X, self.train_y), steps=steps)
        ev = regressor.evaluate(input_fn=lambda: input_fn(self.test_X, self.test_y), steps=1)
        print('  -  Trained Loss: {0:f}'.format(ev["loss"]))

    @staticmethod
    def predict(regressor, input_fn, x_data):
        return regressor.predict(input_fn=lambda: input_fn(x_data))

    def dnn_regressor(self, est_margin=None, plot_filename='', plot_=True, pkl_filename=''):
        regressor, input_fn = self.get_tf_model([96,2])
        self.tf_train(regressor, input_fn, steps=1000000)
        return True


if __name__ == "__main__":

    trans_train_Xy = lib.read_csv_file('train_70.csv')
    trans_test_Xy = lib.read_csv_file('test_30.csv')

    train_X = lib.transpose_list(trans_train_Xy[:-1])
    train_y = trans_train_Xy[-1]

    test_X = lib.transpose_list(trans_test_Xy[:-1])
    test_y = trans_test_Xy[-1]

    dnn = DnnLearner(train_X, train_y, test_X, test_y, name_exist=True)
    dnn.dnn_regressor()


