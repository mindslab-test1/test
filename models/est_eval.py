import configparser
import os
import pickle
from datetime import datetime

from handlers import learning_handler
from lib import learning as learning_lib
from lib import sys_lib as system_lib
from lib import data as data_lib


def get_feat_handler_est_dict(ini, str_dat, category, logger, DB_OP_=True, server_mode='EVAL'):
    feat_handler = system_lib.get_feat_handler(ini['DATA_ANALYTICS']['var_ini_file'],
                                               ini['DATA_ANALYTICS']['var_csv_file'])

    est_db_handler, eval_db_handler = system_lib.create_db_handlers(ini, logger, DB_OP_)
    est_table_info = data_lib.read_csv_file(ini['EST_DB']['table_info_csv'])

    try:
        decode_dict = feat_handler.decode_stream_comma(str_dat, dbg_=False, logger=logger)
    except Exception as e:
        logger.error("FATAL : decode_stream : " + str(e))
        return False

    logger.info("{} {} operation, ({:d}), ({:d}) - {}.".format(category, server_mode, len(str_dat), len(decode_dict), str(decode_dict)))

    # Check variable and process type
    check_status, check_list = feat_handler.check_var_ranges(decode_dict, fix_=True, logger=logger)
    check_process_type = feat_handler.check_var_process_type(decode_dict, cat=category, logger=logger)
    feat_handler.check_process_type = str(check_process_type)

    model_dir = system_lib.get_model_dir(ini['MACHINE_LEARNING_MODEL']['root_dir'],
                                         category,
                                         ini['MACHINE_LEARNING_MODEL']['method'],
                                         ini['MACHINE_LEARNING_MODEL']['version'],
                                         ini['SELF_LEARNING']['result_file'])

    if server_mode == 'EST':
        feat_handler.vars['EST_REQUEST_TIME'].val = system_lib.get_datetime()
        feat_handler.vars['EST_RANGE_CHECK'].val = int(check_status)
        feat_handler.vars['EST_RANGE_CHECK_VARS'].val = str(check_list).replace("\'", "").replace("\"", "")
        feat_handler.vars['EST_METHOD'].val = ini['MACHINE_LEARNING_MODEL']['method']
        feat_handler.vars['EST_MODEL_VER'].val = model_dir
        est_dict = decode_dict
        return feat_handler, est_dict

    elif server_mode == 'EVAL':
        feat_handler.vars['EVAL_START_TIME'].val = system_lib.get_datetime()
        feat_handler.vars['EVAL_RANGE_CHECK'].val = int(check_status)
        feat_handler.vars['EVAL_RANGE_CHECK_VARS'].val = str(check_list).replace("\'", "").replace("\"", "")
        feat_handler.vars['EVAL_METHOD'].val = ini['MACHINE_LEARNING_MODEL']['method']

        # if model_dir is None:
        # Todo: make a model
        feat_handler.vars['EVAL_MODEL_VER'].val = model_dir

        est_dict = None
        if DB_OP_:
            cond_list = ['CHARGE_NO = \'' + decode_dict['CHARGE_NO'] + '\'']
            try:
                est_db = est_db_handler.selectDataByConditions(cond_list=cond_list)[0][1:]
                logger.info("EST_DB read done")
                est_dict = data_lib.convert_db_to_dict(est_db, est_table_info[1:])
            except BaseException as e:
                logger.error(str(cond_list) + " : " + str(e))

        return feat_handler, est_dict


def estimate(ini, args, feat_handler, str_dat, est_dict, logger, DB_OP_=True, server_mode='EST'):
    est_db_handler = estimation(ini, args, feat_handler,
                                est_dict,
                                logger, DB_OP_,
                                server_mode)

    est_table_info = data_lib.read_csv_file(ini['EST_DB']['table_info_csv'])

    if DB_OP_ and str_dat[:8]:
        db_row_dict = data_lib.gen_db_row_dat(est_table_info, feat_handler)
        est_db_handler.insertOne(db_row_dict)
        logger.info("EST_DB read done")
        if True:
            print("EST_DB read back:" + str(est_db_handler.selectOne(est_db_handler.selectLastPK())))


def evaluate(networking_ini, args, feat_handler, str_dat, eval_dict, est_dict, logger, DB_OP_=True):
    dataset, eval_db_handler, learn_msg = evaluation_setup_get_dataset(networking_ini, args, feat_handler,
                                                                       eval_dict, est_dict,
                                                                       logger, DB_OP_)
    if dataset:
        evaluation_start(args, networking_ini, dataset, feat_handler, learn_msg, logger)

    eval_table_info = data_lib.read_csv_file(networking_ini['EVAL_DB']['table_info_csv'])

    if DB_OP_ and str_dat[:8]:
        db_row_dict = data_lib.gen_db_row_dat(eval_table_info, feat_handler)
        eval_db_handler.insertOne(db_row_dict)
        logger.info("EVAL_DB read done")
        if True:
            print("EVAL_DB read back:" + str(eval_db_handler.selectOne(eval_db_handler.selectLastPK())))

        # if args.cat == 'CVT':
        #     eval_row_dict = lib.gen_row_dat(eval_table_info, Feat)
        #     compare_status = lib.compare_est_and_eval_dat(est_dict, eval_row_dict, logger=logger)


def estimation(ini, args, feat_handler, est_dict, logger, DB_OP_=True, server_mode='EST'):

    learning_lib.calculate_ml_output(ini['MACHINE_LEARNING_MODEL']['method'],
                                     args.cat,
                                     feat_handler.vars['EST_MODEL_VER'].val,
                                     est_dict,
                                     feat_handler,
                                     logger,
                                     feat_handler.check_process_type,
                                     server_mode
                                     )

    feat_handler.vars['EST_REPLY_TIME'].val = system_lib.get_datetime()

    est_db_handler = None
    est_db_handler, eval_db_handler = system_lib.create_db_handlers(ini, logger, DB_OP_)

    return est_db_handler


def evaluation_setup_get_dataset(ini, args, feat_handler, eval_dict, est_dict, logger, DB_OP_=True):

    learning_lib.calculate_ml_output(ini['MACHINE_LEARNING_MODEL']['method'],
                                     args.cat,
                                     feat_handler.vars['EVAL_MODEL_VER'].val,
                                     eval_dict,
                                     feat_handler,
                                     logger,
                                     feat_handler.check_process_type,
                                     )

    eval_table_info = data_lib.read_csv_file(ini['EVAL_DB']['table_info_csv'])

    if est_dict:
        ret_status, ret_list = feat_handler.check_var_tolerances(est_dict, eval_dict, logger=logger)
        feat_handler.vars['EVAL_TOLERANCE_CHECK'].val = int(ret_status)
        feat_handler.vars['EVAL_TOLERANCE_CHECK'].val = int(ret_status)
        feat_handler.vars['EVAL_TOLERANCE_CHECK_VARS'].val = str(ret_list).replace("\'", "").replace("\"", "")
        if args.cat == 'CVT' or args.cat == 'RH':
            feat_handler.vars['EVAL_PERF'].val = abs(est_dict['EST_TEMP'] - feat_handler.vars['EVAL_TEMP'].val)
        elif args.cat == 'RH_M6':
            feat_handler.vars['EVAL_PERF'].val = abs(est_dict['EST_TUNDISH_CARBON'] - feat_handler.vars['EVAL_TUNDISH_CARBON'].val)
    else:
        feat_handler.vars['EVAL_TOLERANCE_CHECK'].val = -1
        feat_handler.vars['EVAL_PERF'].val = data_lib.INFINITY
        logger.info("There is no charge no. in estimation DB matched with {}".format(eval_dict['CHARGE_NO']))

    feat_handler.vars['EVAL_END_TIME'].val = system_lib.get_datetime()
    latest_dir = system_lib.get_model_dir(ini['MACHINE_LEARNING_MODEL']['root_dir'],
                                          args.cat,
                                          ini['MACHINE_LEARNING_MODEL']['method'],
                                          ini['MACHINE_LEARNING_MODEL']['version'])
    if not latest_dir:
        logger.error("Latest model directory does not exist.")

    model_date = os.path.split(latest_dir)[-1]

    present = datetime.now()
    past = datetime(int("20" + model_date[:2]), int(model_date[2:-2]), int(model_date[-2:]), 0, 0)

    days_passed = (present - past).days

    days_in_month = system_lib.get_days_in_month(model_date)

    dataset = None
    eval_db_handler = None
    learn_msg = ''

    est_db_handler, eval_db_handler = system_lib.create_db_handlers(ini, logger, DB_OP_)

    if DB_OP_ and system_lib.time_elapsed(ini['SELF_LEARNING']['condition'], days_passed, days_in_month):

        eval_cond_list = ["{0}<{1}".format("20" + model_date, 'TAP_WORK_DATE'),
                          "{0}>{1}".format("30000000", 'TAP_WORK_DATE')]
        db_data = [[]]
        db_data[0] = eval_db_handler.selectDataByConditions(cond_list=eval_cond_list)

        msg = "Get last updated {:d} data from EVAL DB with {}.".format(len(db_data[-1]), eval_cond_list)
        logger.info(msg)
        learn_msg = "\n # " + msg
        db_data[-1].insert(0, [x[0] for x in eval_table_info[1:]])

        system_lib.write_to_db(args.cat, db_data)

        db_data.append(feat_handler.refine_dataset_col_by_dataset_order(db_data[-1], logger=logger))
        learn_msg += "\n # " + feat_handler.log
        db_data.append(feat_handler.refine_dataset_col_by_null_replacement(db_data[-1], logger=logger))
        learn_msg += "\n # " + feat_handler.log
        db_data.append(feat_handler.refine_dataset_row_by_null(db_data[-1], logger=logger))
        learn_msg += "\n # " + feat_handler.log
        db_data.append(feat_handler.refine_dataset_row_by_process_type(db_data[-1], args.cat, logger=logger)) ## 경처리 데이터로 filtering
        learn_msg += "\n # " + feat_handler.log
        db_data.append(feat_handler.refine_dataset_group_by_operation_pattern(db_data[-1], args.cat, logger=logger))  ## 7가지 조업패턴 데이터로 grouping
        learn_msg += "\n # " + feat_handler.log
        db_data.append(feat_handler.refine_dataset_row_by_nan_replacement(db_data[-1], args.cat, logger=logger))  ## nan 데이터 처리
        learn_msg += "\n # " + feat_handler.log
        db_data.append(feat_handler.refine_dataset_row_by_range(db_data[-1], logger=logger))
        learn_msg += "\n # " + feat_handler.log

        dataset = data_lib.read_csv_file(ini['SELF_LEARNING']['dataset_file'])
        org_dataset_len = len(dataset) - 1
        add_dataset_len = len(db_data[-1]) - 1
        # sort by local dataset column
        sorted_db_data = data_lib.extract_rows_from_table(db_data[-1], dataset[0])

        dataset += sorted_db_data[1:]
        data_lib.write_list_to_csv(dataset, ini['SELF_LEARNING']['dataset_file'])

        msg = "Write new self-learning dataset, {:d} + {:d}".format(org_dataset_len, add_dataset_len)
        logger.info(msg)
        learn_msg += "\n # " + msg

    return dataset, eval_db_handler, learn_msg


def evaluation_start(args, ini, dataset, feat_handler, learn_msg, logger):

    today = system_lib.get_datetime("%y%m%d")
    new_model_dir = os.path.join(ini['MACHINE_LEARNING_MODEL']['root_dir'],
                                 args.cat,
                                 ini['MACHINE_LEARNING_MODEL']['method'],
                                 today)

    if not os.path.exists(new_model_dir):
        os.makedirs(new_model_dir)

    ml_cfg = configparser.ConfigParser()
    ml_cfg.read(args.ml_ini_file)

    ml_cfg = configparser.ConfigParser()
    ml_cfg.read(args.ml_ini_file)

    param_dict = {'dataset': dataset}

    learner = learning_handler.MachineLearner(Feat=feat_handler, dataset=dataset, out_idx=0)
    if ini['MACHINE_LEARNING_MODEL']['method'] == "DNN":
        learner.normalize_number_features()
        learner.encode_symbol_dataset_by_one_hot_encoder()
        ml_cfg['DNN']['model_dir'] = os.path.join(new_model_dir, ml_cfg['DNN']['model_dir'])
    elif ini['MACHINE_LEARNING_MODEL']['method'] == "RFR":
        learner.encode_symbol_dataset_by_int()

    bin_dir = ini['MACHINE_LEARNING_MODEL']['method'] + ".bin"

    learner.run(ini['MACHINE_LEARNING_MODEL']['method'], ml_cfg)

    param_dict['params'] = learner.params

    if args.cat == 'RH_PLUS':
        msg = "Time : train accuracy - {:6.2f}, test accuracy - {:6.2f}, Temperature : train accuracy - {:6.2f}, " \
              "test accuracy - {:6.2f}". \
            format(learner.train_acc, learner.test_acc, learner.train_acc_2, learner.test_acc_2)
        logger.info(msg)
        learn_msg += "\n # " + msg
        msg = "Time : train MSE - {:6.2f}, test MSE - {:6.2f}, Temperature : train MSE - {:6.2f}, test MSE - {:6.2f}". \
            format(learner.train_mse, learner.test_mse, learner.train_mse_2, learner.test_mse_2)
        logger.info(msg)
        learn_msg += "\n # " + msg
    else:
        msg = "Train accuracy : {:6.2f}, Test accuracy : {:6.2f}".format(learner.train_acc,
                                                                         learner.test_acc)
        logger.info(msg)
        learn_msg += "\n # " + msg
        msg = "Train MSE : {:6.2f}, Test MSE : {:6.2f}".format(learner.train_mse, learner.test_mse)
        logger.info(msg)
        learn_msg += "\n # " + msg

    if ini['MACHINE_LEARNING_MODEL']['method'] == "DNN":
        pickle.dump(param_dict, open(os.path.join(new_model_dir, bin_dir), "wb"))
    else:
        pickle.dump(learner, open(os.path.join(new_model_dir, bin_dir), "wb"))
    with open(os.path.join(new_model_dir, ini['SELF_LEARNING']['result_file']), "w") as fid:
        fid.write(learn_msg)
