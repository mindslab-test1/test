#! /usr/bin/env python
# -*- coding: utf-8 -*-

import pymssql
from pandas import DataFrame
import pandas as pd
import matplotlib.pyplot as plt

import sys
if sys.version_info >= (3, 3):
    import collections.abc as collections_abc
else:
    import collections as collections_abc

conn = pymssql.connect(host="172.24.92.148", user="SA", password="sm2_ai_dev!", database="MINDS_CVT")
cursor = conn.cursor()
cursor.execute("SELECT No, TRANSACTION_CD, CHARGE_NO, TAP_WORK_DATE, CC_TD_AVG_TEMP "
               "FROM [MINDS_RH].[dbo].[rh_eval] "
               "WHERE TAP_WORK_DATE > 20180101 AND TAP_WORK_DATE < 20210401 "
               "ORDER BY TAP_WORK_DATE desc, No desc ")
row = cursor.fetchall()
row_all = [list(r) for r in row]
column_names = [item[0] for item in cursor.description]
df = DataFrame(row_all, columns = column_names)
print (df)

df2 = df.groupby('TAP_WORK_DATE').min()

def count_zero(column):
    return (column == 0).sum()

df2['count_zero'] = df.groupby('TAP_WORK_DATE')['CC_TD_AVG_TEMP'].apply(count_zero)
df2.to_csv("count_zero.csv", mode='w')