import numpy as np

INFINITY = 999999.0
DIV_0_ERR = "#DIV/0!"


class NumberClass(object):
    """ Class for number feature.

    """

    def __init__(self,
                 name='',
                 if_name='',
                 category='',
                 idx=None,
                 stream_dtype='',
                 stream_length=-1,
                 stream_decimal=-1,
                 stream_position=-1,
                 var_type=None,
                 min_val=None,
                 max_val=None,
                 default_val=None,
                 priority=None,
                 null_is='',
                 tolerance='',
                 dataset_order=-1):
        self.name = name
        self.if_name = if_name
        self.category = category
        self.enum_idx = idx
        self.stream_dtype = stream_dtype
        self.stream_length = int(stream_length) if stream_length else -1
        self.stream_decimal = int(stream_decimal) if stream_decimal else -1
        self.stream_position = int(stream_position) if stream_position else -1
        self.var_type = var_type
        self.min_thresh = min_val
        self.max_thresh = max_val
        self.default_val = default_val
        self.priority = priority
        self.null_is = null_is
        self.tolerance = tolerance if tolerance else [0, 0]
        self.dataset_order = dataset_order

        self.null_num = -1
        self.min_below = -1
        self.max_above = -1
        self.mean = INFINITY
        self.std = INFINITY
        self.val = INFINITY
        self.dat_arr = []

    def get_statistical_properties(self, dat):
        dat = ["" if val == DIV_0_ERR else val for val in dat]
        dat = [self.null_is if val == "" else val for val in dat]
        self.null_num = dat.count('')
        self.min_below = 0
        self.max_above = 0
        dat_arr = []
        for val in dat:
            if val:
                if float(val) < self.min_thresh:
                    self.min_below += 1
                elif self.max_thresh < float(val):
                    self.max_above += 1
                else:
                    dat_arr.append(float(val))

        dat_arr = np.array(dat_arr)
        self.mean = dat_arr.mean()
        self.std = dat_arr.std()
        self.dat_arr = dat_arr


class StringClass(object):
    """ Class for string feature.

    """

    def __init__(self,
                 name='',
                 if_name='',
                 category='',
                 idx=None,
                 stream_dtype='',
                 stream_length=-1,
                 stream_decimal=-1,
                 stream_position=-1,
                 default_val=None,
                 domain='',
                 priority=None,
                 null_is='',
                 dataset_order=-1):
        self.name = name
        self.if_name = if_name
        self.category = category
        self.enum_idx = idx
        self.stream_dtype = stream_dtype
        self.stream_length = int(stream_length) if stream_length else -1
        self.stream_decimal = int(stream_decimal) if stream_decimal else -1
        self.stream_position = int(stream_position) if stream_position else -1
        self.default_val = default_val
        self.domain = domain
        self.priority = priority
        self.null_is = null_is
        self.dataset_order = dataset_order
        self.val = ''

        self.null_num = -1

    def get_statistical_properties(self, dat):
        self.null_num = dat.count("")
