# -*- coding: utf-8 -*-

import pymssql
import csv


class DdlQuery:

    def __init__(self, server_name="192.168.0.7", port="1433", username="SA", password="Posco17temp", db_name="MINDS_POSCO",
                 pk_name="no", logger=None):
        logger.info("DDL init") if logger else print("DDL init")
        self.server_name = server_name
        self.port = port
        self.username = username
        self.password = password
        self.db_name = db_name
        self.pk_name = pk_name

        self.conn = pymssql.connect(self.server_name, self.username, self.password, self.db_name)
        self.cursor = self.conn.cursor()

    def getConnection(self):
        try:
            self.conn = pymssql.connect(self.server_name, self.username, self.password, self.db_name)
            self.cursor = self.conn.cursor()
            print("DDL Query connection start")
            return 'success!!'
        except pymssql.StandardError as e:
            print(e)
            return e

    def createTable(self, table_name, csv_info_file):
        # self.getConnection()

        try:
            with open(csv_info_file, 'rt') as f:
                rdr = csv.reader(f, delimiter=',')

                columnString = ""

                for index, val in enumerate(rdr):
                    if index != 0:
                        columnString += val[0] + " " + val[1] + ", "

                firstQuery = " IF OBJECT_ID('" + table_name + "', 'U') IS NOT NULL " \
                             + " DROP TABLE " + table_name \
                             + " CREATE TABLE " + table_name + "(" \
                             + " No INT identity(1,1) NOT NULL, "

                secondQuery = " primary key (" + self.pk_name + ") )"

                # print(firstQuery + columnString + secondQuery)

                self.cursor.execute(firstQuery + columnString + secondQuery)

                self.conn.commit()
                print("DDL Query connection end")
                # self.conn.close()
            return True
        except pymssql.StandardError as e:
            print(e)
            return False

    def dropTable(self, table_name):
        # self.getConnection()

        try:
            queryString = "IF OBJECT_ID('" + table_name + "', 'U') IS NOT NULL" + " DROP TABLE " + table_name
            self.cursor.execute(queryString)

            self.conn.commit()
            print("DDL Query connection end")
            # self.conn.close()
            return True
        except pymssql.StandardError as e:
            print(e)
            return False

    def createDatabase(self, db_name):
        try:
            queryString = "CREATE DATABASE " + db_name
            # create database 할 때는 커넥션을 따로 생성해야 한다.
            conn = pymssql.connect(self.server_name, self.username, self.password, self.db_name)
            conn.autocommit(True)
            cursor = conn.cursor()
            cursor.execute(queryString)
            conn.close()
            return True
        except pymssql.StandardError as e:
            print(e)
            return False

    def dropDatabase(self, db_name):
        try:
            queryString = "DROP DATABASE " + db_name
            # create database 할 때는 커넥션을 따로 생성해야 한다.
            conn = pymssql.connect(self.server_name, self.username, self.password, self.db_name)
            conn.autocommit(True)
            cursor = conn.cursor()
            cursor.execute(queryString)
            conn.close()
            return True
        except pymssql.StandardError as e:
            print(e)
            return False
