## mssql wrapping with Python

- python 3.5
- install pymssql (pip install pymssql)
- 사용법 참고 (mssql_sample.py)


```python
# -*- coding: utf-8 -*-

import mssql.DDL.query
import mssql.DML.query

# 파라미터로 아래 내용을 보내서 인스턴스를 생성합니다.
ddlQuery = mssql.DDL.query.DdlQuery(
    server_name="127.0.0.1", port="1433", username="test", password="Posco17temp", db_name="MINDS_POSCO"
)
dmlQuery = mssql.DML.query.DmlQuery(
    server_name="127.0.0.1", port="1433", username="test", password="Posco17temp", db_name="MINDS_POSCO"
)

# select tables
# default database('MINDS_POSCO')의 모든 table 조회
# return : 2D List
# print(dmlQuery.selectTables())

# drop table
# parameter : string table_name
# return True / False
# print(ddlQuery.dropTable('POSCO_DATA'))

# create table
# parameter : string table_name, string csv_info_file
# return : True / False
# print(ddlQuery.createTable('POSCO_DATA', 'mssql_columns_info.csv'))

# insert all
# parameter : string table_name, string csv_data_file
# return : True / False
# print(dmlQuery.insertAll('POSCO_DATA', '171010_posco_sinter (edited).csv'))

# selectAll
# parameter : string table_name
# return : 2D List
# print(dmlQuery.selectAll('POSCO_DATA'))

# selectOne
# parameter : string table_name, int pk
# return : 2D List
# print(dmlQuery.selectOne('POSCO_DATA', 1))

# selectLastPK
# parameter : string table_name
# return : int pk
# print(dmlQuery.selectLastPK('POSCO_DATA'))

param_sample = [
    {'col': 'HL_CUR_LEV', 'val': 56.6963089881}
    , {'col': 'EXG_EQP_GAS_TMP', 'val': 204.9548424619}
    , {'col': 'EXG_FRESH_AIR_FLW', 'val': 2200.6432598004}
    , {'col': 'EXG_RECIR_AIR_FLW', 'val': 3392.4509623044}
    , {'col': 'SIN_TOT_AMT', 'val': 631.3625580974}
    , {'col': 'AG_RATIO', 'val': 1.2766258603}
]

# update
# parameter : string table_name, list(dict) param_sample, int pk
# return : True / False
# print(dmlQuery.update('POSCO_DATA', param_sample, 111))

# insertOne
# parameter : string table_name, list(dict) param_sample
# return : True / False
print(dmlQuery.insertOne('POSCO_DATA', param_sample))


```


### 주의사항
- 중복된 컬럼이름이 없어야 합니다.
- 컬럼 이름에는 특수문자가 없어야 합니다.
- 빈 값의 경우 DB에는 null로 들어갑니다.
- mssql_columns_info.csv 파일은 컬럼명, 데이터타입 순서로 만들어야 합니다.
- 각 설정은 mssql-cfg.ini 에 있습니다.

