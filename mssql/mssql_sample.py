# -*- coding: utf-8 -*-

import configparser
import mssql.pymssqlwrapper

config = configparser.ConfigParser()
config.read('gen_db.ini')

database = mssql.pymssqlwrapper.PyMssqlWrapper(
    server_name=config['db']['server_name']
    , port=config['db']['port']
    , username=config['db']['username']
    , password=config['db']['password']
    , db_name=config['db']['db_name']
    , table_name=config['db']['post_table_name']
)

# print(database.selectAll())

# database.createDatabase('test_db')

# database.createTable(config['db']['post_table_name'], config['db']['post_table_info_csv'])
# database.insertAll(config['db']['post_dataset_csv'], config['db']['post_table_name'])

database.selectDataByConditions('rh_pre', [['CC_AVR_STEEL_WGT', '<14423']])
