# -*- coding: utf-8 -*-

import pymssql
import csv


class DmlQuery:

    def __init__(self, server_name="192.168.0.7", port="1433", username="SA", password="Posco17temp", db_name="MINDS_POSCO",
                 pk_name="no", logger=None):
        logger.info("DML init") if logger else print("DML init")
        self.server_name = server_name
        self.port = port
        self.username = username
        self.password = password
        self.db_name = db_name
        self.pk_name = pk_name

        self.conn = pymssql.connect(self.server_name, self.username, self.password, self.db_name)
        self.cursor = self.conn.cursor()

    def selectColumnType(self, table_name):
        try:
            queryString = "SELECT column_name, data_type FROM information_schema.columns WHERE TABLE_NAME = '" + table_name + "'"
            # print(queryString)
            self.cursor.execute(queryString)

            returnValue = []
            for item in self.cursor.fetchall():
                returnValue.append(list(item))

            # print(returnValue)
            return returnValue
        except pymssql.StandardError as e:
            print(e)
            return e

    def insertAll(self, table_name, csv_data_file):
        try:
            db_columns = self.selectColumnType(table_name)
            varchar_col_indexes = []

            with open(csv_data_file, 'rt') as f:
                rdr = csv.reader(f, delimiter=',')
                columnString = ""
                doInsert = False

                for index, val in enumerate(rdr):
                    valueString = ""

                    # NULL 예외처리
                    for i, v in enumerate(val):
                        if v == '':
                            val[i] = 'null'
                        # column type 검사 및 varchar 일 때 list에 index 넣기
                        for j, row in enumerate(db_columns):
                            if row[1] == 'varchar':
                                if index == 0:
                                    if row[0] == val[i]:
                                        varchar_col_indexes.append(i)

                        if index != 0:
                            for row in varchar_col_indexes:
                                if i == row:
                                    val[i] = "'" + val[i] + "'"

                    if index == 0:
                        columnString += ",".join(val)
                    else:
                        doInsert = True
                        valueString = ",".join(val)

                    firstQuery = " INSERT INTO " + table_name + "(" \
                                 + columnString + ") VALUES (" \
                                 + valueString
                    secondQuery = " )"

                    if doInsert:
                        print(firstQuery + secondQuery)
                        self.cursor.execute(firstQuery + secondQuery)

                self.conn.commit()
                # self.conn.close()
            return True
        except pymssql.StandardError as e:
            print(e)
            return False

    def selectAll(self, table_name):
        try:
            queryString = "SELECT * FROM " + table_name
            self.cursor.execute(queryString)
            # print(self.cursor.fetchall())

            returnValue = []
            for item in self.cursor.fetchall():
                returnValue.append(list(item))

            # self.conn.close()
            return returnValue
        except pymssql.StandardError as e:
            print(e)
            return e

    def selectOne(self, table_name, no):
        try:
            queryString = "SELECT * FROM " + table_name + " WHERE " + self.pk_name + " = '" + str(no) +"'"
            self.cursor.execute(queryString)
            # print(self.cursor.fetchall())

            returnValue = []
            for item in self.cursor.fetchall():
                returnValue.append(list(item))

            # self.conn.close()
            return returnValue
        except pymssql.StandardError as e:
            print(e)
            return e

    def selectLastPK(self, table_name):
        try:
            queryString = "SELECT TOP 1 " + self.pk_name + " FROM " + table_name + " ORDER BY " + self.pk_name + " DESC "
            self.cursor.execute(queryString)

            returnValue = self.cursor.fetchone()
            # self.conn.close()
            return int(returnValue[0])
        except pymssql.StandardError as e:
            print(e)
            return e

    def update(self, table_name, params, no):   # HOON: list is a built-in function in Python.  I changed it to query_list.
        try:
            query_list = []
            firstQuery = "UPDATE " + table_name + " SET "
            for i, v in enumerate(params):
                paramRow = v['col'] + " = " + str(v['val'])
                query_list.append(paramRow)

            paramString = ", ".join(query_list)
            secondQuery = " WHERE " + self.pk_name + "= " + str(no)

            self.cursor.execute(firstQuery + paramString + secondQuery)
            self.conn.commit()
            # self.conn.close()
            return True
        except pymssql.StandardError as e:
            print(e)
            return False

    def insertOne(self, table_name, param_dict, logger=None):
        try:
            # no = self.selectLastPK(table_name) + 1
            colList = list()
            valList = list()

            for key in param_dict.keys():
                # print(key)
                colList.append(key)

            for value in param_dict.values():
                valList.append(str(value))

            colString = ", ".join(colList)
            valString = ", ".join(valList)

            queryString = "INSERT INTO " + table_name \
                          + " ( " + colString + " ) VALUES ( " \
                          + valString + " ) "

            if logger:
                logger.info(queryString)
            else:
                # print(queryString)
                pass

            self.cursor.execute(queryString)
            self.conn.commit()
            # self.conn.close()
            return True
        except pymssql.StandardError as e:
            print(e)
            return False

    def selectTables(self):
        try:
            # queryString = "SELECT * FROM " + self.database["db_name"] + ".information_schema.tables "
            queryString = "SELECT table_name FROM " + self.db_name + ".information_schema.tables "
            self.cursor.execute(queryString)
            returnValue = []
            for item in self.cursor.fetchall():
                returnValue.append(list(item))

            # self.conn.close()
            return returnValue
        except pymssql.StandardError as e:
            print(e)
            return e

    # TODO: import column name to top row
    def exportDataToCsv(self, table_name, csv_data_file):
        try:
            data = self.selectAll(table_name)
            with open(csv_data_file, 'w', newline='') as csvfile:
                spamwriter = csv.writer(csvfile)
                spamwriter.writerows(data)
            return True
        except pymssql.StandardError as e:
            print(e)
            return False

    def deleteOne(self, table_name, no):
        try:
            queryString = "DELETE FROM " + table_name + " WHERE " + self.pk_name + "= " + str(no)
            print(queryString)
            self.cursor.execute(queryString)
            self.conn.commit()
            return True
        except pymssql.StandardError as e:
            print(e)
            return False

    def selectColumnNames(self, table_name):
        try:
            queryString = "SELECT column_name FROM information_schema.columns WHERE TABLE_NAME = '" + table_name + "'"
            # print(queryString)
            self.cursor.execute(queryString)

            returnValue = []
            for item in self.cursor.fetchall():
                returnValue.append(list(item))

            # print(returnValue)
            return returnValue
        except pymssql.StandardError as e:
            print(e)
            return e

    #
    # input: [[COL1, "==100"], [COL2, "<25%"]]
    # output: Database Selected Data
    #
    def selectDataByConditions(self, table_name, col_list, cond_list, sort_col='', sort_desc=True, logger=None):
        try:
            condition_str = " WHERE 1=1 "
            if cond_list:
                for cond in cond_list:
                    condition_str += ' AND ' + cond

            queryString = "SELECT " + col_list \
                          + " FROM " + table_name \
                          + condition_str
            if sort_col:
                queryString += " ORDER BY " + sort_col
                if sort_desc:
                    queryString += " desc "
                else:
                    queryString += " asc "
            if logger:
                logger.info(queryString)
            else:
                # print(queryString)
                pass
            self.cursor.execute(queryString)

            returnValue = []
            for item in self.cursor.fetchall():
                returnValue.append(list(item))

            # print(returnValue)
            return returnValue
        except pymssql.StandardError as e:
            print(e)
            return e
